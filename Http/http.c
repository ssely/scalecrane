#ifndef HTTP_C_
#define HTTP_C_
#define  _WIN32_WINNT  0x0501
#define  WINVER  0x0501
#define _WIN32_IE  0x0600


#include <stdio.h>
#include <string.h>
#include "..\inc\mongoose.h"
#include "..\inc\scale.h"
#include "..\inc\version.h"
#include "..\inc\sqlite3.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"



PCHAR acl,Buf,dbpath;
BOOL debug=FALSE; //������/������� (1/0) ����������� ����

pCRANEINFO pCri=NULL;


struct mg_mgr mgr;
static const char *s_http_port;
static struct mg_serve_http_opts s_http_server_opts;

int _get_request_var(LPSTR k,PCHAR src, PCHAR dst, int max_len)
{
int rc=0,len,slen,i,find=0;
        len = lstrlen(k);
        slen = lstrlen(src)-len;
        for(i=0;i<slen;i++)
            if(!strnicmp((const char*)k,(const char *)(src+i),len))
                if(src[len]=='=')
                    {
                        find=len+1;
                        break;
                    }
    if(find)
        for(i=find;i<slen;i++)
            {
            if((src[i]=='&') || ((src[i]==' ') && (src[i+1]=='H') && (src[i+2]=='T') && (src[i+3]=='T') && (src[i+4]=='P')))
                {
                dst[i]=0;
                break;
                }
            if(i==max_len)
                {
                dst[rc]=0;
                break;
                }
            dst[rc++]=src[i];
            }
return rc;
}

void url_decode(char *dst,  const char *src)
{
        char a, b;
        while (*src) {
                if ((*src == '%') &&
                    ((a = src[1]) && (b = src[2])) &&
                    (isxdigit(a) && isxdigit(b))) {
                        if (a >= 'a')
                                a -= 'a'-'A';
                        if (a >= 'A')
                                a -= ('A' - 10);
                        else
                                a -= '0';
                        if (b >= 'a')
                                b -= 'a'-'A';
                        if (b >= 'A')
                                b -= ('A' - 10);
                        else
                                b -= '0';
                        *dst++ = 16*a+b;
                        src+=3;
                } else if (*src == '+') {
                        *dst++ = ' ';
                        src++;
                } else {
                        *dst++ = *src++;
                }
        }
        *dst++ = '\0';
}

void _strip_bracket(LPSTR Buf)
{
int i,rc;
        rc = lstrlen(Buf);
        for(i=0;i<rc;i++)
            if((CHAR)Buf[i] == '(')
            {
            Buf[i] = 0;
            break;
            }
}

static void ev_handler(struct mg_connection *nc, int ev, void *p) {
struct http_message * hm;
PCHAR page,req;
PCHAR sqlName,colName;
int cc,len,i,col;
DWORD num;
sqlite3 *sl;
sqlite3_stmt *res;
time_t isNow;
struct tm *nt;

hm = (struct http_message *) p;
if(ev == MG_EV_ACCEPT)
	{
	// ��������� ������ ����������� �������
	cc=mg_check_ip_acl((const char *) acl, htonl(nc->sa.sin.sin_addr.S_un.S_addr));
	if(cc < 1)
		{
		nc->flags |= MG_F_SEND_AND_CLOSE;
		cc = mg_conn_addr_to_str(nc, Buf, 0x20,MG_SOCK_STRINGIFY_IP|MG_SOCK_STRINGIFY_REMOTE);
		if(debug)
            printf("%10lu | %s ���������� ���������\n",(DWORD)time(NULL),Buf);
        lstrcat(Buf," ���������� ���������");
		WriteLogWeb((LPSTR) Buf, pCri->ID, TRUE);
		}
	return;
	}
  if (ev == MG_EV_HTTP_REQUEST) {
// ��������� ������ ����������� �������
	cc=mg_check_ip_acl((const char *) acl, htonl(nc->sa.sin.sin_addr.S_un.S_addr));
// ���� �� �������� ������, �� �� �����
	if(cc < 1)
		{
	    nc->flags |= MG_F_SEND_AND_CLOSE;
   		cc = mg_conn_addr_to_str(nc, Buf, 0x20,MG_SOCK_STRINGIFY_IP|MG_SOCK_STRINGIFY_REMOTE);
		if(debug)
            printf("%10lu | %s ���������� ���������\n",(DWORD)time(NULL),Buf);
		lstrcat(Buf," ���������� ���������");
		WriteLogWeb((LPSTR) Buf, pCri->ID, TRUE);
		return;
		}

//--------------------------
// ������ ������
    InterlockedExchange((volatile LONG *) &num,pCri->ID);
    Buf[0]=0;
	page=(char *)malloc_w(0x1000);
	req=(char *)malloc_w(0x1000);
	//len = mg_get_http_var(&hm->query_string, "string", req,0x800);
	if(hm->query_string.len)
        url_decode( (char*) page,  (const char *) hm->query_string.p);
	lstrcpy(req,"string");
	len = _get_request_var((LPSTR)req,page , (PCHAR) req, 0x800);
//	cc = mg_url_decode(req, len, hm->query_string.p, hm->query_string.len,0);
//	cc = _replace_str((PCHAR) req, '_', 0x20);
	cc = mg_conn_addr_to_str(nc, page, 0x20,MG_SOCK_STRINGIFY_IP|MG_SOCK_STRINGIFY_REMOTE);
//	sprintf(Buf,"%10lu | �����:%s\t| ������:%.*s\n",(DWORD)time(NULL),page,(int) len+5,req);

	if(debug)
        printf("%10lu | �����:%s\t| ������:%.*s\n",(DWORD)time(NULL),page,(int) len,req);

    sprintf(Buf,"�����:%s\t������:%.*s",page,(int) len,req);
    WriteLogWeb((LPSTR) Buf, num, TRUE);

    isNow=time(NULL);
	nt = localtime(&isNow);

    mg_printf(nc,"HTTP/1.1 200 OK\r\nContent-Type: text/xml\r\nTransfer-Encoding: chunked\r\n\r\n");
    mg_printf_http_chunk(nc,"<?xml version=\"1.0\" encoding=\"Windows-1251\"?>");
    mg_printf_http_chunk(nc,"<scale id=\"%02u\" now=\"%02u.%02u.%04u %02u:%02u:%02u\" weight=\"%s\" scoop=\"%d\" operation=\"%d\" station=\"%lu\" flags=\"%lu\" >",num,nt->tm_mday,nt->tm_mon+1,nt->tm_year+1900,nt->tm_hour,nt->tm_min,nt->tm_sec,pCri->WE[0] ? pCri->WE:"none",pCri->ID_Scoop,pCri->Operation,pCri->ID_Station,pCri->Flags);
    if((GetFileAttributes(dbpath)!=INVALID_FILE_ATTRIBUTES))
        if((cc = sqlite3_open_v2((const char *)dbpath,&sl,SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE,NULL))==SQLITE_OK)
                {
                    if(req[0]=='\"')
                        req[0]=0x20;
                    if(req[len-1]=='\"')
                        req[len-1]=0x20;

                if((cc = sqlite3_prepare_v2(sl, req, -1, &res, 0)) == SQLITE_OK)
                    {
					res=NULL;
					res = sqlite3_next_stmt(sl, NULL);
                    col = sqlite3_column_count(res);
	                    while((cc = sqlite3_step(res))==SQLITE_ROW)
    	                    {
                            mg_printf_http_chunk(nc,"<crane>");
                            Buf[0]=0;
    	                        for(i=0;i<col;i++)
                                {
                                    sqlName = (PCHAR)sqlite3_column_text(res, i);
                                    colName = (PCHAR)sqlite3_column_name(res, i);
                                    _strip_bracket((LPSTR) colName);
                                    sprintf(page,"<%s>%s</%s>",colName,sqlName,colName);
                                    lstrcat(Buf,page);
                                }
                            if(debug)
                                printf("<crane>%s</crane>\n",Buf);
                            cc = _replace_str((PCHAR) Buf, '\'', '\"');
                            mg_printf_http_chunk(nc,"%s</crane>",Buf);
                        	}
                		sqlite3_finalize(res);
                    }
                cc = sqlite3_close_v2(sl);
                }
    mg_printf_http_chunk(nc,"</scale>");
    nc->flags |= MG_F_SEND_AND_CLOSE;
    mg_send_http_chunk(nc, "", 0);

    free_w(req);
	free_w(page);
  }
}




int _get_param(PCHAR ac,PCHAR port, PCHAR list, PCHAR dp, PBYTE num, BOOL* debug, BOOL* quit,PCHAR IniFile)
{
static BOOL isFirst=FALSE;
int rc=0,cc;
PCHAR CurDir;

CurDir=(PCHAR) malloc_w(0x200);
GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
*num=(BYTE)GetPrivateProfileInt("scale","num",0,(LPCSTR)CurDir);
GetPrivateProfileString("http","acl","-0.0.0.0",ac,0x100,(LPCSTR)CurDir);
    _alltrim(ac,ac);
    _copy_str_until(ac,ac,';');
GetPrivateProfileString("http","listing","no",list,0x10,(LPCSTR)CurDir);
    _alltrim(list,list);
    _copy_str_until(list,list,';');
GetPrivateProfileString("http","dbpath","store.db",dp,0x100,(LPCSTR)CurDir);
    _alltrim(dp,dp);
    _copy_str_until(dp,dp,';');
cc=GetPrivateProfileInt("http","port",80,(LPCSTR)CurDir);
wsprintf(port,"%i",cc);
cc=GetPrivateProfileInt("http","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
*quit=(BOOL)GetPrivateProfileInt("http","exit",0,(LPCSTR)CurDir);

if(!isFirst)
{
	wsprintf(Buf,"Debug HTTP Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) Buf );
	printf("������������ HTTP �� �����: %s\n",(PCHAR)CurDir);
	printf("����� �����:\t\t%03d\n",(INT) *num);
	printf("������ ������� ACL:\t%s\n", ac);
	printf("���� HTTP �������:\t%s\n", port);
	printf("���������� ��������:\t%s\n",list);
	printf("���������� �������:\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);
}
if(!(isFirst=*debug))
		_closeConsole();

free_w(CurDir);
return rc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
FileMap fm;
PCHAR docRoot,listing;
BYTE num;
PCHAR cfg;
BOOL quit=FALSE,lock=FALSE;
int cc;
HANDLE isRun;

struct mg_connection *nc;

Buf=(PCHAR) malloc_w(0x1000);
acl=(PCHAR)malloc_w(0x1000);
s_http_port=(PCHAR)malloc_w(0x10);
listing=(PCHAR)malloc_w(0x10);
docRoot=(PCHAR)malloc_w(0x100);
dbpath=(PCHAR)malloc_w(0x200);
cfg = (PCHAR)malloc_w(0x200);

if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
    {
    free_w(cfg);
    free_w(dbpath);
    free_w(docRoot);
    free_w(listing);
    free_w(s_http_port);
    free_w(acl);
    free_w(Buf);
    return 1;
    }

cc=_get_param(acl,(PCHAR)s_http_port,listing,dbpath, &num, &debug, &quit,cfg);
//debug=FALSE;

wsprintf((LPSTR)Buf,"Global\\Http%03d",num);
if((isRun = _chkIsRun((LPSTR)Buf))==NULL)
    {
		wsprintf(Buf,"Debug HTTP Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) Buf );
        printf("��������� HTTP -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    free_w(cfg);
    free_w(dbpath);
    free_w(docRoot);
    free_w(listing);
    free_w(s_http_port);
    free_w(acl);
    free_w(Buf);
    return FALSE;
    }
// ������ ������� ��� ������� � Web �������
	wsprintf(docRoot,".\\%02d",num);
	if((GetFileAttributes(docRoot)==INVALID_FILE_ATTRIBUTES))
			CreateDirectory(docRoot, NULL);

// �������������� HTTP ������
  mg_mgr_init(&mgr, NULL);

  nc = mg_bind(&mgr, s_http_port, ev_handler);


  if (nc == NULL) {
    free_w(cfg);
    free_w(dbpath);
    free_w(docRoot);
    free_w(listing);
    free_w(s_http_port);
    free_w(acl);
    free_w(Buf);
    CloseHandle(isRun);
    return 1;
  }

  // Set up HTTP server parameters
  mg_set_protocol_http_websocket(nc);
  s_http_server_opts.document_root = ".";  // Serve current directory
  s_http_server_opts.enable_directory_listing = listing;

pCri=(pCRANEINFO)_createExchangeObj(num, &fm);

if(pCri==NULL)
{
mg_mgr_free(&mgr);
free_w(cfg);
free_w(dbpath);
free_w(docRoot);
free_w(listing);
free_w(s_http_port);
free_w(acl);
free_w(Buf);
CloseHandle(isRun);
return 2;
}

if(!pCri->ID)
	InterlockedExchange16((PWORD) &pCri->ID,(WORD)num);

while(!quit)
{
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param(acl,(PCHAR)s_http_port,listing,dbpath, &num, &debug, &quit,cfg);
            if(cc & 0x2){
                if(debug){
					wsprintf(Buf,"Debug HTTP Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) Buf);
                    printf("HTTP ���� :%s ������� ��������:\"%s\" ����:%u\n",(PCHAR)s_http_port,listing, (unsigned short) num);
                }
                else
					_closeConsole();
//			mg_mgr_free(&mgr);
			}
        }

mg_mgr_poll(&mgr, 200);
		if((!(time(NULL)%5)) && (!lock))
			{
			lock=TRUE;
			if(debug)
				printf("%lu|����:%02u|WE:%s|SC:%u|OP:%u|Fl:0x%lX\n",time(NULL),pCri->ID,pCri->WE,(DWORD)pCri->ID_Scoop,pCri->Operation,(DWORD)pCri->Flags);
			}
			else if(time(NULL)%5)
				lock=FALSE;
}

if(debug)
    _closeConsole();

// ���� ��������� ����������
/*InterlockedExchange8((PBYTE) &pWi->Quit,0);
InterlockedExchange8((PBYTE) &pWi->CarKey,0);
InterlockedExchange8((PBYTE) &pWi->ZeroKey,0);
InterlockedExchange8((PBYTE) &pWi->Bell,0);
InterlockedExchange8((PBYTE) &pWi->Semaphore,0);
InterlockedExchange8((PBYTE) &pWi->Light,0);*/
//--------------------------

mg_mgr_free(&mgr);
_destroyExchangeObj(num, &fm);
free_w(cfg);
free_w(dbpath);
free_w(docRoot);
free_w(listing);
free_w(s_http_port);
free_w(acl);
free_w(Buf);
CloseHandle(isRun);
return 0;
}
#endif //HTTP_C_
