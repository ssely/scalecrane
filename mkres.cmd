@echo off
setlocal disabledelayedexpansion

echo mkres %1

set DAY=%date:~0,2%
set MON=%date:~3,2%
set YEAR=%date:~6,4%

FOR /F "tokens=1,2,3,4,5,6,7,8,9,10,11 delims=:" %%i in (version.inf) do (
set MAJOR=%%i
set MAJOR_VAR=%%j
set MINOR=%%k
set MINOR_VAR=%%l
set BUILD=%%m
set BUILD_VAR=%%n
set REV=%%o
set REV_VAR=%%p
set BUILD_COUNT=%%q
set BUILD_COUNT_VAR=%%r
set RELEASE=%%s
)
echo 1 VERSIONINFO >resource.rc
echo FILEVERSION %MAJOR_VAR%, %MINOR_VAR%, %BUILD_VAR%, %REV_VAR% >>resource.rc
echo PRODUCTVERSION %MAJOR_VAR%, %MINOR_VAR%, %REV_VAR%, %BUILD_COUNT_VAR% >>resource.rc
rem FILEOS VOS_NT_WINDOWS32
echo FILETYPE 0x00000001L  >>resource.rc
echo {  >>resource.rc
echo  BLOCK "StringFileInfo" >>resource.rc
echo  {  >>resource.rc
echo   BLOCK "040904e4"  >>resource.rc
echo   {  >>resource.rc
echo          VALUE "FileDescription",    "%1 - ������ ������ � ������" >>resource.rc
echo          VALUE "ProductName",        "���� �� ������ ���������� ��" >>resource.rc
echo          VALUE "ProductVersion",     "%MAJOR_VAR%, %MINOR_VAR%, %REV_VAR%, %BUILD_COUNT_VAR%" >>resource.rc
echo          VALUE "CompanyName",        "��� ""MicroPC Lab""�" >>resource.rc
echo          VALUE "LegalTrademarks",    "��� ��� ��""������""�" >>resource.rc
echo          VALUE "LegalCopyright",     "��� ""MicroPC Lab""�" >>resource.rc
echo          VALUE "OriginalFilename",   "%1.exe �� %DAY%.%MON%.%YEAR%" >>resource.rc
echo   } >>resource.rc
echo } >>resource.rc
echo  BLOCK "VarFileInfo" >>resource.rc
echo  { >>resource.rc
echo   VALUE "Translation", 0x0419, 0x04E3 >>resource.rc
echo  } >>resource.rc
echo } >>resource.rc
windres resource.rc  -O coff -o resource.res
if [%2] == [] exit