#ifndef _udp2mat_h
#define _udp2mat_h

extern int UDP_Init(void);
extern int UDP_Close(void);
extern int UDP_Send(PCHAR,UINT16,PVOID,int);
extern int UDP_OpenSend(void);
extern int UDP_OpenReceive(void);
extern int UDP_Receive(UINT16,int,PVOID);
extern DWORD UDP_Receive_Timed(UINT16,int,PVOID,DWORD,PWORD);
#endif //_udp2mat_h
