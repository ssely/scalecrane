#pragma pack (push,1)
typedef struct{
DWORD ID_Scoop; //0-3; ����� �����
long Operation;   //4-7; ��� ��������
long Weight_1; //8-11; ����� ����� ����
DWORD Weight_2;// 12-15; ������� ����� ����
DWORD Flags; // 16-19; �����������, ����������� � ������ �����
DWORD ID_Station; // 740-743; ����� ������������
DWORD ID; // 20-23; ����� �����
CHAR WE[0x10];  //24-39; ASCII ��������� WE 2110
CHAR Store[0x5][0x80]; // 39-679; � ����� ������ ����������� ��������
DWORD MqttError; // 680-683;  ����� ������� ����� �� ��������� MQTT
DWORD Reserv[0xE]; // 684-739; ������ 15-�� ������� ����
CHAR Tablo[0x10]; // 744-759 ���������� ��� �����
}CRANEINFO, *pCRANEINFO;

typedef struct{
  HANDLE hFile; // ����������  �����
  HANDLE hMapping; // ����������  ����� � ������
  size_t fsize;  // ������ ����� � ������
  PBYTE dataPtr; // ��������� �� ������ ��� ����������� ����������� �����
} FileMap, *pFileMap;

#pragma pack (pop)

#define malloc_w(m) HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS|HEAP_ZERO_MEMORY, (m))
#define free_w(m)  HeapFree(GetProcessHeap(),0,(LPVOID) (m))

#define VT_WEIGHT 1 // ������� �������� �� �����
#define VT_ZERO 2  // �� ����� "0"
#define LOSTWE 6  // ������ ����� � ������� ��������


#ifndef MISC_C
extern PVOID _createExchangeObj(int, pFileMap);
extern void _destroyExchangeObj(int, pFileMap);
extern BOOL _chkConfig(LPSTR );
extern int  _find_cmd_variable(PCHAR, PCHAR, PCHAR);
extern void _openConsole(LPSTR);
extern void _closeConsole(void);
extern void WriteLogWeb(LPSTR,int,BOOL);
extern BYTE InterlockedExchange8(CHAR volatile *, CHAR );
extern SHORT InterlockedExchange16(SHORT volatile *, SHORT Value);
extern BYTE R_TRIG32(BYTE,BYTE);
extern BYTE F_TRIG32(BYTE,BYTE);
extern INT  _DeleteLog(int ,int );
extern HANDLE _chkIsRun(LPSTR);
extern BOOL isNoLogin(LPSTR);
extern BOOL _setFlags(PDWORD, BYTE, BOOL);
extern BOOL _getFlags(PDWORD, BYTE);
extern UINT16 hex2dec(PCHAR);
extern int _replace_str(PCHAR, CHAR, CHAR);
extern int _alltrim(PCHAR,PCHAR);
extern int _copy_str_until(PCHAR,PCHAR, CHAR);
#endif //MISC_C
