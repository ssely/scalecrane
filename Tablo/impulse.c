#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\scale.h"
#include "..\inc\udp2mat.h"


#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

#define VT_WEIGHT 1
#define VT_ZERO 2
#define LOSTWE 6

CHAR ReqString[0x100];


int _get_param(PCHAR ip_addr,int* cmdport,PDWORD timeout,PBYTE num, BOOL* debug, BOOL* quit,PCHAR IniFile)
{
CHAR ip[0x10]={0};  // ip ����� ����
CHAR CurDir[0x100];
/*
int cmdport;    //���� ��� ������ ������ �� ����� �������
DWORD timeout; //������� �������� ������ � ����� � �������������
BOOL debug; //������/������� (1/0) ����������� ����
BOOL quit; // ���������� �������� ������
*/
static BOOL isFirst=FALSE;
int rc=0,cc;

GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("tablo","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
*cmdport=GetPrivateProfileInt("tablo","cmdport",4003,(LPCSTR)CurDir);
*timeout=(DWORD)GetPrivateProfileInt("tablo","delay",1000,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("scale","num",1,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("tablo","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
if(!isFirst)
{
	wsprintf(ReqString,"Debug ����� ������� Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) ReqString );
	printf("������������ ����� ������� �� �����: %s\n",(PCHAR)CurDir);
	printf("IP �����:\t\t\t%s\n",(PCHAR)ip);
	printf("���� ������:\t\t\t%d\n",(INT) *cmdport);
	printf("������� �������� ������:\t%lu ms\n", *timeout);
    printf("����� �����:\t\t\t%03d\n",(INT) *num);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);

}

if(!(isFirst=*debug))
		_closeConsole();

*quit=(BOOL)GetPrivateProfileInt("tablo","exit",0,(LPCSTR)CurDir);
return rc;
}
int _mkBuffer(PBYTE Dst, PBYTE Src,int Len)
{
    int rc,i=1;
    rc = lstrlen((LPCSTR)Src);
    Dst[0]='$';
    if((Src[rc-2]=='.') || (Src[rc-3]=='.'))
        {
            Dst[i++]=' ';
            Len++;
        }
    rc = Len - rc;

    for(i;i<Len;i++)
    {
        Dst[i]=' ';
        if(i >= rc)
            Dst[i] = Src[i-rc];
        Dst[i+1]=0;
    }

return (lstrlen(Dst));
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    DWORD ttick,summ=0UL,isNow;
    CHAR ipaddr[0x10]; // ����� ����
    int cmdport;    //���� ��� ������ ������ �� ������ WVK2000
    DWORD timeout; //������� �������� ������ � ����� � �������������
    BYTE num;   //����� ������������ �����
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc,i=0,ErrorLink=0,cnt=0;
    FileMap fm;
	pCRANEINFO cri;
    PBYTE dataExchange;
    CHAR cfg[0x40];
    BYTE Buffer[0x40];
	HANDLE isRun;
    DWORD timeReceive;

if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
 		return FALSE;


cc=_get_param((PCHAR) ipaddr,&cmdport,&timeout,&num,&debug, &quit,cfg);
debug=0;

wsprintf((LPSTR)ReqString,"Global\\Tablo%03d",num);
if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
		wsprintf(ReqString,"Debug Tablo Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) ReqString );
        printf("��������� Tablo -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

if(debug)
	printf("Mutex %s is open\n",ReqString);

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
cri=(pCRANEINFO)fm.dataPtr;

if(debug)
	printf("Exchange Object is open:%p\n",dataExchange);

if(cri->ID!=num)
	InterlockedExchange16((volatile SHORT *) &cri->ID,(WORD)num);

	cc=UDP_Init();
if(debug)
	printf("Udp Init:%d\n",cc);

    cc=UDP_OpenSend();
if(debug)
	printf("Udp Send Init:%d\n",cc);
/*
    cc=UDP_OpenReceive();
if(debug)
	printf("Udp Reseive Init:%d\n",cc);
*/
//	InterlockedExchange16((volatile SHORT *) &wi->VT_Num,(WORD)vtnum);
//	InterlockedExchange8((volatile PCHAR) &wi->Request,1);
isNow=time(NULL);
InterlockedExchange((volatile LONG *) &cri->Flags,0UL);
while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&cmdport,&timeout,&num,&debug,&quit,cfg);
            if(cc & 0x2){
                if(debug){
                    sprintf(ReqString,"Debug Tablo Request Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) ReqString );
                    printf("Tablo �����:%s WE2110 ��������� ����:%u\n",ipaddr,cmdport);
                }
                else
					_closeConsole();
            }
        }
    timeReceive=GetTickCount();
    i=0;
    ttick=timeout;

    if(cnt>1000)
        cnt=0; // �����;

 //   wsprintf(cri->Tablo,"%d",cnt++);
//    if(cri->Weight_1 >=0 )
//        wsprintf(Buffer,"$   -1.23",cri->Tablo);
//    else
//        wsprintf(Buffer,"$%4s",cri->Tablo);
//wsprintf(cri->Tablo,"%s","123.4");
        cc = _mkBuffer(Buffer, cri->Tablo,8);
//        cc = _mkBuffer(Buffer, cri->Tablo,6);
    //cc=UDP_Send((PCHAR)ipaddr,(int) cmdport,"$" , 1);
	cc=UDP_Send((PCHAR)ipaddr,(int) cmdport,Buffer , lstrlen(Buffer));
	cc=UDP_Send((PCHAR)ipaddr,(int) cmdport,"\r\n" ,2);
	if(debug&& ((time(NULL)-isNow)>=1))
		{
		isNow=time(NULL);
		printf("%lu|����:%02u|�������:%04lu|Fl:0x%lX|%s;%d\n",isNow,num,ttick,cri->Flags,Buffer,(int)cri->Flags&1);
    	}
	Sleep(timeout);
	}

if(!debug)
		_closeConsole();

	cc=UDP_Close();
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
