#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef GLOBALMAP_C
#define GLOBALMAP_C


#include <stdio.h>
#include <time.h>
#include <math.h>
#include <windows.h>
#include ".\inc\scale.h"


#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

int main(int ac, PCHAR * av)
{
UINT16 num;
PBYTE dataExchange;
FileMap fm;


if(ac > 1)
	num = atoi(av[1]);
else
	num = 1001;

dataExchange=(PBYTE)_createExchangeObj(num, &fm);


printf("Exchange Object is open:%p\n",dataExchange);

_destroyExchangeObj(num, &fm);
printf("Exchange Object is close:%p\n",dataExchange);
Sleep(5000UL);
return num;
}
#endif // GLOBALMAP_C
