#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "..\inc\mqtt.h"
#include "..\inc\posix_sockets.h"
#include "..\inc\scale.h"
#include "..\inc\version.h"
#include <windows.h>
//#include <wininet.h>

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"


//#define DEBUG

#define MOTION 0
#define SAVEDB 1
#define LEFT 2
#define RIGHT 3
#define UP 4
#define DOWN 5
#define LOSTWE 6
#define LOSTDIO 7

CHAR MqttString[0x100];
//CHAR FullPath[0x100];

int _get_param(PCHAR is_ip,PCHAR is_port,PCHAR top,PCHAR user,PCHAR pass,PBYTE num,UINT16 *qs,BOOL *rt,BOOL *dbg,BOOL *is_quit,PCHAR IniFile)
{
 PCHAR CurDir;
int rc=0,cc;
UINT16 port,qos;

CurDir=(PCHAR) malloc_w(0x200);
GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
lstrcpy((LPSTR)IniFile,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("scale","num",0,(LPCSTR)CurDir);
GetPrivateProfileString("mqtt","ip","127.0.0.1",is_ip,16,(LPCSTR)CurDir);
port=(UINT16)GetPrivateProfileInt("mqtt","port",123,(LPCSTR)CurDir);
wsprintf(is_port,"%u",port);
GetPrivateProfileString("mqtt","tag","Crane",top,0x20,(LPCSTR)CurDir);
GetPrivateProfileString("mqtt","paranoiaU","",user,127,(LPCSTR)CurDir);
GetPrivateProfileString("mqtt","paranoiaP","",pass,127,(LPCSTR)CurDir);
qos=(UINT16)GetPrivateProfileInt("mqtt","qos",0,(LPCSTR)CurDir);
*qs = ((qos << 1) & 0x06);
*rt=(BOOL)GetPrivateProfileInt("mqtt","route",0,(LPCSTR)CurDir);
*dbg=(BOOL)GetPrivateProfileInt("mqtt","debug",0,(LPCSTR)CurDir);
*is_quit=(BOOL)GetPrivateProfileInt("mqtt","exit",0,(LPCSTR)CurDir);

_openConsole((LPSTR) MqttString );
	printf("������������ MQTT �� �����:\t\t%s\n",(PCHAR)CurDir);
	printf("IP ����� MQTT:\t\t\t\t%s\n",(PCHAR)is_ip);
	printf("MQTT ����:\t\t\t\t%s\n",(PCHAR) is_port);
	printf("MQTT topic:\t\t\t\t%s\n",(PCHAR)top);
	printf("MQTT user:\t\t\t\t%s\n",user[0] >0 ? "********":"");
	printf("MQTT pass:\t\t\t\t%s\n",pass[0] >0 ? "********":"");
	printf("MQTT route:\t\t\t\t%s\n",*rt? "���":"����");
	printf("QOS:\t\t\t\t\t%d\n",(UINT16) qos);
    printf("���������� �������:\t\t\t%s\n", *dbg ? "���" : "����");
	Sleep(3000UL);
_closeConsole();

free_w(CurDir);

rc = (user[0] > 0) || (pass[0] > 0);
return rc;
}

UINT16 _mkpublish(struct mqtt_client *mqc,PCHAR top,pCRANEINFO prev,pCRANEINFO curr,uint8_t qos,BOOL d)
{
static BOOL lock=FALSE,fl=FALSE;
CHAR tp[0x80],_wrk[0x100];
UINT rc=0;
enum MQTTErrors session;
static DWORD now,prv=0,timer=0UL;
SYSTEMTIME lt,gt;
DWORD p,c;
BOOL isTime=FALSE;

        now = GetTickCount();

        if((now-prv)>5000UL)
			{
			isTime=TRUE;
			prv=GetTickCount();
			}

//         GetLocalTime(&lt);
         GetSystemTime(&lt);
        if((prev->Flags!=curr->Flags) || isTime)
        {
            p=prev->Flags;
            c=curr->Flags;
            if((_getFlags(&(prev->Flags),MOTION)!=_getFlags(&(curr->Flags),MOTION)) || isTime)
            {
            wsprintf(tp,"/%s/MotionWE",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,_getFlags(&(prev->Flags),MOTION)>0 ? 0:1);
//            wsprintf(_wrk,"%u",_getFlags(&(prev->Flags),MOTION)>0 ? 0:1);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("MotionWE:%u\n",_getFlags(&(prev->Flags),MOTION)>0 ? 0:1);
            }
            if((_getFlags(&(prev->Flags),SAVEDB)!=_getFlags(&(curr->Flags),SAVEDB)) || isTime)
            {
            wsprintf(tp,"/%s/StoreData",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,_getFlags(&(prev->Flags),SAVEDB)>0 ? 1:0);
//            wsprintf(_wrk,"%u",_getFlags(&(prev->Flags),SAVEDB)>0 ? 0:1);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
				{
                printf("StoreData:%u\n",_getFlags(&(prev->Flags),SAVEDB)>0 ? 0:1);
				printf(_wrk,"StoreData:%u\n",_getFlags(&(prev->Flags),SAVEDB)>0 ? 0:1);
				}
            }
            if((_getFlags(&(prev->Flags),LEFT)!=_getFlags(&(curr->Flags),LEFT)) || isTime)
            {
            wsprintf(tp,"/%s/MoveLeft",top);

//            wsprintf(_wrk,"%u",_getFlags(&(prev->Flags),LEFT)>0 ? 1:0);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,_getFlags(&(prev->Flags),LEFT)>0 ? 1:0);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("MoveLeft:%u\n",_getFlags(&(prev->Flags),LEFT)>0 ? 1:0);
            }
            if((_getFlags(&(prev->Flags),RIGHT)!=_getFlags(&(curr->Flags),RIGHT)) || isTime)
            {
            wsprintf(tp,"/%s/MoveRight",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,_getFlags(&(prev->Flags),RIGHT)>0 ? 1:0);
//            wsprintf(_wrk,"%u",_getFlags(&(prev->Flags),RIGHT)>0 ? 1:0);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("MoveRight%u\n",_getFlags(&(prev->Flags),RIGHT)>0 ? 1:0);
            }
            if((_getFlags(&(prev->Flags),UP)!=_getFlags(&(curr->Flags),UP)) || isTime)
            {
            wsprintf(tp,"/%s/MoveUp",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,_getFlags(&(prev->Flags),UP)>0 ? 1:0);
//            wsprintf(_wrk,"%u",_getFlags(&(prev->Flags),UP)>0 ? 1:0);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("MoveUp:%u\n",_getFlags(&(prev->Flags),UP)>0 ? 1:0);
            }
            if((_getFlags(&(prev->Flags),DOWN)!=_getFlags(&(curr->Flags),DOWN)) || isTime)
            {
            wsprintf(tp,"/%s/MoveDown",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,_getFlags(&(prev->Flags),DOWN)>0 ? 1:0);
//            wsprintf(_wrk,"%u",_getFlags(&(prev->Flags),DOWN)>0 ? 1:0);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("MoveDown:%u\n",_getFlags(&(prev->Flags),DOWN)>0 ? 1:0);
            }
            if((_getFlags(&(prev->Flags),LOSTWE)!=_getFlags(&(curr->Flags),LOSTWE)) || isTime)
            {
            wsprintf(tp,"/%s/LostWE",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,_getFlags(&(prev->Flags),LOSTWE)>0 ? 1:0);
//            wsprintf(_wrk,"%u",_getFlags(&(prev->Flags),LOSTWE)>0 ? 1:0);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("LostWE:%u\n",_getFlags(&(prev->Flags),LOSTWE)>0 ? 1:0);
            }
            if((_getFlags(&(prev->Flags),LOSTDIO)!=_getFlags(&(curr->Flags),LOSTDIO)) || isTime)
            {
            wsprintf(tp,"/%s/LostDIO",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,_getFlags(&(prev->Flags),LOSTDIO)>0 ? 1:0);
//            wsprintf(_wrk,"%u",_getFlags(&(prev->Flags),LOSTDIO)>0 ? 1:0);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("LostDIO:%u\n",_getFlags(&(prev->Flags),LOSTDIO)>0 ? 1:0);
            }
            curr->Flags=prev->Flags;
#ifdef DEBUG
            prev->Flags = (now>>4);
#endif
            rc = 1;
        }

        if((prev->ID_Scoop!=curr->ID_Scoop) || isTime)
        {
            wsprintf(tp,"/%s/Scoop",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,(WORD)prev->ID_Scoop);
//            wsprintf(_wrk,"%u",(WORD)prev->ID_Scoop);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("Scoop:%u\n",(WORD)prev->ID_Scoop);
            curr->ID_Scoop=prev->ID_Scoop;
#ifdef DEBUG
            prev->ID_Scoop = (now>>4)&0x7F;
#endif
            rc = 1;
        }

        if((prev->Operation!=curr->Operation) || isTime)
        {
            wsprintf(tp,"/%s/Operation",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,(WORD)prev->Operation);
//            wsprintf(_wrk,"%u",(WORD)prev->Operation);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("Operation:%u\n",(WORD)prev->Operation);
            curr->Operation=prev->Operation;
            rc = 1;
        }
        if((prev->ID_Station!=curr->ID_Station)  || isTime)
        {
            wsprintf(tp,"/%s/Station",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,(WORD)prev->ID_Station);
//            wsprintf(_wrk,"%u",(WORD)prev->ID_Station);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("Station:%u\n",(WORD)prev->ID_Station);
            curr->ID_Station=prev->ID_Station;
            rc = 1;
        }
		if((lstrcmp(curr->WE,prev->WE)) || isTime)
		{
            wsprintf(tp,"/%s/weight",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %s",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,prev->WE);
//            wsprintf(_wrk, "%s",prev->WE);

            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            lstrcpy((LPSTR)curr->WE,(LPCSTR)prev->WE);
            rc = 1;
		}
#ifdef DEBUG
        prev->Operation=(now>>5) & 3;
        prev->ID_Station=(now>>6) & 7;
#endif
        if((timer != time(NULL)) || rc)
        {
//            wsprintf(tp,"/%s/timestamp",top);
//            GetLocalTime(&lt);
//            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %s",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,curr->WE);
//            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
//			if(d)
//				printf("Stamp:%s\n",_wrk);
#ifdef DEBUG
            p=(now - ((now/1000000UL)*1000000UL))/10000UL;
            wsprintf(tp,"/%s/weight",top);
            wsprintf(_wrk, "%2i.%03i",99UL-p,now%1000);

            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            lstrcpy((LPSTR)prev->WE,(LPCSTR)_wrk);
			if(d)
                printf("Wight:%s\n",prev->WE);
#endif
/*
            wsprintf(tp,"/%s/Scoop",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %u",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,(WORD)prev->ID_Scoop);
//            wsprintf(_wrk,"%u",(WORD)prev->ID_Scoop);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);
            if(d)
                printf("Scoop:%u\n",(WORD)prev->ID_Scoop);

            wsprintf(tp,"/%s/Operation",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %lu",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,(DWORD)prev->Operation);
//            wsprintf(_wrk,"%lu",(DWORD)prev->MqttError);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);

            if(d)
                printf("Operation:%lu\n",(DWORD)prev->Operation);
*/
            wsprintf(tp,"/%s/ErrorTime",top);
            wsprintf(_wrk,"%u%02u%02u%02u%02u%02u%03u %lu",lt.wYear,lt.wMonth,lt.wDay,lt.wHour,lt.wMinute,lt.wSecond,lt.wMilliseconds,(DWORD)prev->MqttError);
//            wsprintf(_wrk,"%lu",(DWORD)prev->MqttError);
            session = mqtt_publish(mqc, tp, _wrk, lstrlen(_wrk), qos);

            if(d)
                printf("MqttError:%lu\nWeight:%s\n",(DWORD)prev->MqttError,prev->WE);


			timer=time(NULL);
  //          rc = 1;
        }

#ifdef DEBUG
        if((!(timer%5)) && (!lock))
        {
            if((prev->ID_Station++) > 5)
                prev->ID_Station=1;
            if((prev->Operation++) >4)
                prev->Operation=1;
            if((prev->ID_Scoop++) >20)
                prev->ID_Scoop=1;
        _setFlags(&(prev->Flags),UP,fl);
        _setFlags(&(prev->Flags),DOWN,fl^1);
        _setFlags(&(prev->Flags),MOTION,fl^1);
        _setFlags(&(prev->Flags),SAVEDB,fl);
        fl ^=TRUE;
        lock=TRUE;
        }
        else if((timer%5) && lock)
            lock=FALSE;
#endif

if(rc)
    printf("--------------------------------\n");
return rc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
    WSADATA wsaData;
    int iResult;
    int err;
    DWORD wsaErr;

    PCHAR addr;
    PCHAR port;
    PCHAR topic;
    PCHAR usr;
    PCHAR pass;
    BYTE num;

    int sockfd,i=0;
    struct mqtt_client client;
    PBYTE sendbuf; /* sendbuf should be large enough to hold multiple whole mqtt messages */
    PBYTE recvbuf; /* recvbuf should be large enough any whole mqtt message expected to be received */
    const char* client_id;
    uint8_t connect_flags = MQTT_CONNECT_CLEAN_SESSION;//|MQTT_CONNECT_USER_NAME|MQTT_CONNECT_PASSWORD;
    uint8_t qos = MQTT_PUBLISH_QOS_0;
    enum MQTTErrors ms,session;
    time_t timer;
    struct tm* tm_info;
    PCHAR timebuf;
    UINT16 prevSec=0;
    DWORD Tbegin;
    HANDLE isRun;
//    SYSTEMTIME lt;

    BOOL debug=FALSE,dbg=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������
    BOOL route=FALSE; // ������������� MQTT
    BOOL fl=FALSE;
    int cc;

    FileMap fm;
    pCRANEINFO pCri;
    CRANEINFO cCri;
    PBYTE dataExchange;
    CHAR cfg[0x100];


wsprintf(MqttString, "Debug MQTT Console %s", SC_UBUNTU_FULLVERSION);


if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
    {
	_openConsole((LPSTR) MqttString);
	printf("Unable open config file %s", cfg);
	Sleep(5000UL);
	_closeConsole();
    return FALSE;
    }


    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != NO_ERROR) {
		_openConsole((LPSTR) MqttString);
		printf("Failed to init sockets: %i\n", iResult);
		Sleep(5000UL);
		_closeConsole();
        return iResult;
    }

    sendbuf = malloc_w(0x100000);
    recvbuf = malloc_w(0x80000);
    timebuf = malloc_w(0x80);
    client_id = malloc_w(0x100);

    addr = malloc_w(0x80);
    port = malloc_w(0x80);
    topic= malloc_w(0x80);
    usr =  malloc_w(0x80);
    pass = malloc_w(0x80);


    if((cc = _get_param((PCHAR)addr,(PCHAR)port,(PCHAR)topic,(PCHAR)usr,(PCHAR)pass,(PBYTE) &num,(PUINT16)&qos,&route,&debug,&quit,(PCHAR)cfg)) >0)
        connect_flags |= (MQTT_CONNECT_USER_NAME|MQTT_CONNECT_PASSWORD);

wsprintf((LPSTR)MqttString,"Global\\MQTT%03d",num);
if(!route)
    if((isRun = _chkIsRun((LPSTR)MqttString))==NULL)
    {
		wsprintf(MqttString,"Debug MQTT Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) MqttString );
        printf("��������� MQTT -conf=%s  ��� ��������. �������...",cfg);
        Sleep(5000UL);
        _closeConsole();
    free_w(pass);
    free_w(usr);
    free_w(topic);
    free_w(port);
    free_w(addr);

    free_w(client_id);
    free_w(timebuf);
    free_w(recvbuf);
    free_w(sendbuf);
    WSACleanup();
    return FALSE;
    }
_chkConfig((LPSTR) cfg);
wprintf("dataExchange try open...\n");

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
pCri=(pCRANEINFO)fm.dataPtr;
wprintf("dataExchange opened...\n");
if(pCri->ID!=num)
	InterlockedExchange((volatile DWORD *) &pCri->ID,(DWORD)num);

//wsprintf(topic,"$Scale%03u",num);
wsprintf((LPSTR)client_id,"MQTT_Publisher_%03u",num);

    if((sockfd = open_nb_socket(addr, port))>0)
    {
        ms = mqtt_init(&client, sockfd, sendbuf, sizeof(CRANEINFO)<<4, recvbuf, 0x7FF, NULL);
        ms = mqtt_connect(&client, client_id, NULL, NULL, 0, usr, pass, connect_flags, 1);
    }else{
 		wsprintf(MqttString,"Debug MQTT Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) MqttString );
        printf("������ �������� ������:%d. �������...",sockfd);
        Sleep(5000UL);
        _closeConsole();
    free_w(pass);
    free_w(usr);
    free_w(topic);
    free_w(port);
    free_w(addr);

    free_w(client_id);
    free_w(timebuf);
    free_w(recvbuf);
    free_w(sendbuf);
    WSACleanup();
    return FALSE;
    }

   Tbegin=GetTickCount();
    cCri.Flags=0xFFFFFFFF;
    pCri->MqttError=cCri.MqttError=(DWORD)time(NULL);


    while(!quit)
    {
        if(debug!=dbg)
        {
            if(debug){
                _openConsole((LPSTR) MqttString);
                wsprintf(MqttString,"Debug MQTT Console: %s",SC_UBUNTU_FULLVERSION);
            }
            else
                _closeConsole();
        dbg=debug;
        }


        _mkpublish(&client,topic,pCri,&cCri,qos,debug);

    if((ms = mqtt_sync(&client)) != MQTT_OK)
        {
        close(sockfd);
        mqtt_disconnect (&client);
        client.inspector_callback=NULL;
        pCri->MqttError=(DWORD)time(NULL); // ��������� ������ �����
        if((sockfd = open_nb_socket(addr, port)) > 0)
            {
            mqtt_init(&client, sockfd, sendbuf, sizeof(CRANEINFO)<<4, recvbuf, 0x7FF, NULL);
            ms = mqtt_connect(&client, client_id, NULL, NULL, 0, usr, pass, connect_flags,1);
//            client.inspector_callback = insp_callback;
            }
        }
        else if(pCri->MqttError!=cCri.MqttError)
                cCri.MqttError=pCri->MqttError; // ���������� ������ �����
//    wsaErr = WSAGetLastError();
    Sleep(250UL);
    if(_chkConfig((LPSTR) cfg))
        {
        quit = (UINT16)GetPrivateProfileInt("mqtt","exit",0,(LPCSTR)cfg);
        debug= (UINT16)GetPrivateProfileInt("mqtt","debug",0,(LPCSTR)cfg);
        }
    }
    close(sockfd);
    CloseHandle(isRun);
    if(debug)
        _closeConsole();
    ms = mqtt_disconnect (&client);
    free_w(pass);
    free_w(usr);
    free_w(topic);
    free_w(port);
    free_w(addr);

    free_w(client_id);
    free_w(timebuf);
    free_w(recvbuf);
    free_w(sendbuf);
    WSACleanup();

    _destroyExchangeObj(num, &fm);

    return 0;
}
