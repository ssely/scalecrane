#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef MBGATE_C
#define MBGATE_C

#include <stdio.h>
#include <time.h>
#include <ws2tcpip.h>
#include <winsock2.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\modbus.h"

#include "..\inc\scale.h"


#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wpointer-sign"

#define COILS 32
#define HOLD_REG 14

#define NB_CONNECTION    5
#define MIRRORENTER 0x11 // ������� ����� ���������� ��� MasterScada

CHAR ReqString[0x100];

//--------------

int _get_param(PCHAR ip_addr,int* dataport,PBYTE number,PDWORD tm,BOOL* debug, BOOL* quit,PCHAR IniFile)
{
PCHAR ip;  // ip ����� ����
PCHAR CurDir;
static BOOL isFirst=FALSE;
int rc=0,cc;

ip = (PCHAR) malloc_w(0x100);
CurDir = (PCHAR) malloc_w(0x200);

GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("mbgate","ip","127.0.0.1",ip,0xFF,(LPCSTR)CurDir);
_alltrim(ip,ip);
_copy_str_until(ip,ip,';');
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
*dataport=GetPrivateProfileInt("mbgate","dataport",1502,(LPCSTR)CurDir);
*number=(BYTE)GetPrivateProfileInt("scale","num",0,(LPCSTR)CurDir);
*tm=GetPrivateProfileInt("mbgate","timeout",250,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("mbgate","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
if(!isFirst)
{
	wsprintf(ReqString,"Debug ModBus Server Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) ReqString );
	printf("������������ ModBus Server �� �����: %s\n",(PCHAR)CurDir);
	printf("IP �����:\t\t\t%s\n",(PCHAR)ip);
	printf("���� ������:\t\t\t%d\n",(INT) *dataport);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);

}

if(!(isFirst=*debug))
		_closeConsole();

*quit=(BOOL)GetPrivateProfileInt("mbgate","exit",0,(LPCSTR)CurDir);
free_w(CurDir);
free_w(ip);
return rc;
}

int _fill_hold_registers(pCRANEINFO pc, modbus_mapping_t *reg, BOOL dir)
{
DWORD dwVal;
WORD as;
int i=0,j;
    if(dir){
        InterlockedExchange((volatile LONG *) &dwVal,pc->ID);
        reg->tab_registers[i++]=(UINT16) dwVal;
        InterlockedExchange((volatile LONG *) &dwVal,pc->ID_Scoop);
        reg->tab_registers[i++]=(UINT16) dwVal;
        InterlockedExchange((volatile LONG *) &dwVal,pc->Operation);
        reg->tab_registers[i++]=(UINT16) dwVal;
        InterlockedExchange((volatile LONG *) &dwVal,pc->Weight_1);
        dwVal = 32768;
        reg->tab_registers[i++]=(UINT16) (dwVal>>0x10);
        reg->tab_registers[i++]=(UINT16) (dwVal & 0xFFFF);
//        InterlockedExchange((volatile LONG *) &dwVal,pc->Weight_2);

        InterlockedExchange((volatile LONG *) &dwVal,pc->ID);
        reg->tab_registers[i++]=(UINT16) dwVal;
 /*       for(j=0;j<32;j++)
            {
            InterlockedExchange16((PWORD) &as,(WORD)(pc->WE[j]));
            if(!(j&1))
                reg->tab_registers[i]=(as<<8);
            else
                reg->tab_registers[i++]|=(as&0xFF);
            }
*/
    }
return i;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    CHAR ipaddr[0x10]; // ����� ����
    int dataport;  // ���� ��� ��������� ������ � Modbus
    BYTE num;   //����� ������������ �����
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    BOOL closeconn=TRUE;
    FileMap fm;
	pCRANEINFO pCri;
    PBYTE dataExchange;
    PWORD mbInfo;
    CHAR cfg[0x40],FC;
    PCHAR context;
    DWORD tout;
	HANDLE isRun;
    modbus_t *ctx;
	struct tm *nt;
	time_t isNow;
	int cc,i;
	SOCKET s=-1,master_socket;
	int fdmax,newfd;
    socklen_t addrlen;
    struct sockaddr_in clientaddr;
    modbus_mapping_t *mb_mapping = NULL;
    UINT8 query[MODBUS_TCP_MAX_ADU_LENGTH];
    fd_set refset;
    fd_set rdset;

if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
 		return FALSE;


cc=_get_param((PCHAR) ipaddr,&dataport, &num, &tout,&debug, &quit,cfg);
debug=0;

wsprintf((LPSTR)ReqString,"Global\\MbGate%03d",num);
if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
		wsprintf(ReqString,"Debug ModBus Server Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) ReqString );
        printf("��������� MbGate -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

if(debug)
	printf("Mutex %s is open\n",ReqString);

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
pCri=(pCRANEINFO)fm.dataPtr;

if(debug)
	printf("Exchange Object is open:%p\n",dataExchange);

if(pCri->ID!=num)
    InterlockedExchange16((PWORD) &pCri->ID,(WORD)num);

ctx = modbus_new_tcp(ipaddr,dataport);
modbus_set_debug(ctx,debug);
modbus_set_slave(ctx, 1);
s = (SOCKET) modbus_tcp_listen(ctx, NB_CONNECTION);
mb_mapping = modbus_mapping_new(COILS,0,HOLD_REG,0);
// ��������� �������� �������
if(s==INVALID_SOCKET)
    {
	if(debug){
        printf("Create modbus server error. Bye...\n");
        Sleep(3000UL);
        _closeConsole();

	}
    modbus_mapping_free(mb_mapping);
    modbus_close(ctx);
    modbus_free(ctx);
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    }
    FD_ZERO(&refset);
    FD_SET(s, &refset);
    fdmax = s;

while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&dataport,&num,&tout,&debug,&quit,cfg);
            if(cc & 0x2){
                if(debug){
                    sprintf(ReqString,"Debug ModBus Request Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) ReqString );
                    printf("ModBus �����:%s  ModBus ���� :%u\n",ipaddr,dataport);
                }
                else
					_closeConsole();
            }
        }
        rdset = refset;
        if (select(fdmax+1, &rdset, NULL, NULL, NULL) == -1) {
            if(debug)
                printf("Server select() failure.\n");
            break;
        }

        modbus_set_debug(ctx,debug);
        for (master_socket = 0; master_socket <= fdmax; master_socket++){
            if(!FD_ISSET(master_socket, &rdset))
                continue;
            if(master_socket == s)
			{
                addrlen = sizeof(clientaddr);
                memset(&clientaddr, 0, sizeof(clientaddr));
                newfd = accept(s, (struct sockaddr *)&clientaddr, &addrlen);
                if (newfd == -1)
				{
                    if(debug)
                        printf("Server accept() error\n");
                }
                else
				{
                    FD_SET(newfd, &refset);
                    if(newfd > fdmax)
                        fdmax = newfd;
                    if(debug)
                        printf("New connection from %s:%d on socket %d\n",inet_ntoa(clientaddr.sin_addr), clientaddr.sin_port, newfd);
                }
            }
            else{
                modbus_set_socket(ctx, master_socket);
                if((cc = modbus_receive(ctx, query)) > 0)
                    {
                    isNow=time(NULL);
                    nt = localtime(&isNow);
                    if(debug)
                        printf("Connect:%02u.%02u.%04u %02u:%02u:%02u from %s:%d\n",nt->tm_mday,nt->tm_mon+1,nt->tm_year+1900,nt->tm_hour,nt->tm_min,nt->tm_sec,inet_ntoa(clientaddr.sin_addr), clientaddr.sin_port);
                    FC = query[7];
                    mbInfo = (PWORD) pCri;

               if((FC==15) || (FC==5) || (FC==16))
                    modbus_reply(ctx, query, cc, mb_mapping);

                    if((FC==15) || (FC==5) || (FC==1))
                    {

                        for(i=0;i<COILS;i++)
                            if((i==1) && (FC==15))
                                _setFlags(&pCri->Flags,(BYTE)i,(BOOL)mb_mapping->tab_bits[i]);
                            else
                                mb_mapping->tab_bits[i]=_getFlags(&pCri->Flags,(BYTE)i);

                    }
                  	if((FC==3) || (FC==16))
                    	for(i=0;i<HOLD_REG;i++)
                        	switch(i)
                        	{
                        	case 4:
                        	case 5:
                            case 8:
                        	case 9:
                        	case 12:
                        	case 13:mb_mapping->tab_registers[i] = mbInfo[i]; break;
                        	default:mbInfo[i] = mb_mapping->tab_registers[i]; break;
                        	}
                  	if((FC==3) || (FC==1))
                        modbus_reply(ctx, query, cc, mb_mapping);

                    }
                else if(cc == -1)
                    {
                        if(debug)
                            printf("Connection closed on socket %d\n", master_socket);
                        closesocket(master_socket);
                        FD_CLR(master_socket, &refset);

                        if (master_socket == fdmax)
                                fdmax--;
                    }
            }
        }
	 Sleep(100UL);
	}

	if(!debug)
        _closeConsole();

    modbus_mapping_free(mb_mapping);
    if (s != -1)
        closesocket(s);
    if(master_socket!=1)
        closesocket(master_socket);
    modbus_close(ctx);
    modbus_free(ctx);

	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // MBGATE_C
