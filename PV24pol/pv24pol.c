///
/// <meta lang="ru" charset="windows-1251" emacsmode='-*- markdown -*-'>
///

#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef PV24POL_C
#define PV24POL_C

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <winsock2.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\scale.h"


#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wmisleading-indentation"

/// # ������
///
/// ������ ������ ������������ ��� ������ � �������� ��������� WE21xx/��24
/// �� 485 ���������� � ������� ����������.
/// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// ����������� � �� �������������� �� �� �����
///
/// ************************************************************************
/// *                            .-.
/// *                         .-+   |
/// *                     .--+       '---.                 .-----------.
/// *                    | ���� ��������� |               | WE21xx/��24 |
/// *                     '--------------'                 '-----------'
/// *                          ^   ^                               ^
/// *                          |   |                               |
/// *  .------.   Ethernet     |   |             Ethernet          |
/// *  |  ��  |<--------------'	    '-----------------------------'
/// *  '------'
/// *
/// ************************************************************************


/// ## �������������
///
/// ������ ������ ������������ �� ��������� ������
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
///  PV24Pol64X.exe -conf=���_ini_�����
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// � __ini__ ����� ������ ������������ ������ __[scale]__.
/// ������ ������ ������� ����:
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// [scale]
/// ip="10.0.6.116"
/// dataport=23
/// cmdport=4001
/// timeout=500
/// cmdtimeout=15
/// pktlen=11
/// num=8
/// vtnum=31
/// limit=10
/// debug=1
/// exit=0
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
///
/// �������� � ������ | ��������
/// ------------------|--------------------------------------------------------------
/// __ip__            | ����� �������� ��������� ��-24
/// __dataport__      | TCP ���� ����� ������� ������� ��� � ��
/// __cmdport__       | TCP ���� ����� ������� ���������� ������� � �������� �������
/// __timeout__       | ������� �������� ������ � ����� � �������������
/// __cmdtimeout__    | ������� ������ ������ �� ������� ������ � �������������
/// __pktlen__        | ����� ������������ ������ �� �������� ������� (����)
/// __num__           | ����� ������������ �����
/// __vtnum=31__      | ����� ������� WE21xx/��24 ��� ������ �� �������
/// __limit__         | ��� (��) ��� ���������� �������� ���� ��������� ������.
/// __debug__         | ������/������� (1/0) ����������� ����
/// __exit__          | 1 - ���������� ������ ������
///


CHAR ReqString[0x100];


int _get_param(PCHAR ip_addr,PUINT16 cmdport,PUINT16 dataport,PDWORD timeout, PDWORD cmdtimeout, PBYTE num, PBYTE vtnum,PBYTE pl,BOOL* debug, BOOL* quit,PDWORD lim,PCHAR IniFile)
{
CHAR ip[0x100]={0};  // ip ����� ����
CHAR CurDir[0x100];
/*
int cmdport;    //���� ��� ������ ������ �� ������ WVK2000
int dataport;  // ���� ��� ��������� ������ � WVK2000
DWORD timeout; //������� �������� ������ � ����� � �������������
DWORD cmdtimeout; //������� ������ ������ �� WVK2000 � �������������
BYTE num;   //����� ������������ �����
BYTE vtnum; //����� ������� WVK2000 ��� ������ �� �������
BOOL debug; //������/������� (1/0) ����������� ����
BOOL quit; // ���������� �������� ������
*/
static BOOL isFirst=FALSE;
int rc=0,cc;

GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("scale","ip","127.0.0.1",ip,0xFF,(LPCSTR)CurDir);
_alltrim(ip,ip);
_copy_str_until(ip,ip,';');

    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
//        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        if(ip[0]=='"')
            _copy_str_until((PCHAR) ip_addr,(PCHAR) (ip+1), '"');
        else
            _copy_str_until((PCHAR) ip_addr,(PCHAR) ip, 0x20);
        rc|=1;
    }
*dataport=(UINT16)GetPrivateProfileInt("scale","dataport",8002,(LPCSTR)CurDir);
*cmdport=(UINT16)GetPrivateProfileInt("scale","cmdport",8001,(LPCSTR)CurDir);
*timeout=(DWORD)GetPrivateProfileInt("scale","timeout",5000,(LPCSTR)CurDir);
*cmdtimeout=(DWORD)GetPrivateProfileInt("scale","cmdtimeout",75,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("scale","num",1,(LPCSTR)CurDir);
*vtnum=(BYTE)GetPrivateProfileInt("scale","vtnum",65,(LPCSTR)CurDir);
*pl=(BYTE)GetPrivateProfileInt("scale","pktlen",11,(LPCSTR)CurDir);
*lim=(DWORD)GetPrivateProfileInt("scale","limit",500,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("scale","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
if(!isFirst)
{
	wsprintf(ReqString,"Debug WE21pol Request Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) ReqString );
	printf("������������ WE21pol Auto �� �����: %s\n",(PCHAR)CurDir);
	printf("IP �����:\t\t\t%s\n",(PCHAR)ip);
	printf("���� ������:\t\t\t%d\n",(INT) *dataport);
	printf("���� ������:\t\t\t%d\n",(INT) *cmdport);
	printf("������� �������� ������:\t%lu ms\n", *timeout);
	printf("������� �������� �������:\t%lu ms\n", *cmdtimeout);
	printf("����� �����:\t\t\t%03d\n",(INT) *num);
	printf("����� �������:\t\t\t%03d\n",(INT) *vtnum);
	printf("������ ������:\t\t\t%03d\n",(INT) *pl);
	printf("����� ���������:\t\t%lu ��.\n", *lim);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);

}

if(!(isFirst=*debug))
		_closeConsole();

*quit=(BOOL)GetPrivateProfileInt("scale","exit",0,(LPCSTR)CurDir);
return rc;
}

void tcp_close(SOCKET s, BOOL s_only)
{
struct linger l={1,0};
PCHAR _buff;
//l.l_linger=0; l.l_onoff=1;
//l.l_linger=1;
//l.l_onoff=0;
/*
shutdown(_Client,2);
while(recv(_Client, _buff, 1024, 0)!=-1);
linger l; l.l_linger=0; l.l_onoff=1;
setsockopt(_Client,SOL_SOCKET,SO_LINGER,&l,sizeof(l));
closesocket(_Client);
*/
    if(s_only)
    {
        shutdown(s,SD_BOTH);
		_buff=(PCHAR)malloc_w(0x800);
		while(recv(s, _buff, 1024, 0)!=-1);
		free_w(_buff);
		setsockopt(s,SOL_SOCKET,SO_LINGER,(const char *) &l,sizeof(l));
        Sleep(50UL);
        closesocket(s);
    }
    else
        WSACleanup();
}

// ������ �������
SOCKET tcp_connect(LPSTR Addr, UINT Port,int *Error)
{
    WSADATA wsa;
    static BOOL isFirst=TRUE;
    int rc;
    DWORD iTime = 0UL,Timed;
    SOCKET s=INVALID_SOCKET;
    SOCKADDR_IN sa;
    *Error = 0;

// ���� ������ ������
    if(isFirst)
    {
        if((rc = WSAStartup(MAKEWORD(0x2,0x2),&wsa)))
        {
            *Error = WSAGetLastError();
            return INVALID_SOCKET;
        }
        else
            isFirst=FALSE;
    }

// ______________________
    if((s = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP))==INVALID_SOCKET)
    {
        *Error = WSAGetLastError();
        return INVALID_SOCKET;
    }
    sa.sin_family=AF_INET;
    sa.sin_port=(WORD) htons(Port);
    sa.sin_addr.s_addr = inet_addr(Addr);

    rc = ioctlsocket(s, FIONBIO, &iTime); // block socket
    iTime = 200UL;
    rc = setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char *)&iTime,sizeof(DWORD));
    if((rc = connect(s,(SOCKADDR *) &sa,sizeof(SOCKADDR))))
    {
        *Error = WSAGetLastError();
        tcp_close(s, TRUE);
        s = INVALID_SOCKET;
        return INVALID_SOCKET;
    }
    *Error = WSAGetLastError();
    return s;
}



INT16 tcp_receive(SOCKET s,PBYTE _b,int len,DWORD _t)
{
    static INT8 ErrConnect=0;
    DWORD iTime=200UL, Timed=0UL;
    INT16 Idx=0,i=0,cc;

    if(s==INVALID_SOCKET)
        return 0;

//    cc = ioctlsocket(s, FIONBIO, &isBlock); // non block socket
    Sleep(1UL);
	iTime=_t;
    cc = setsockopt(s,SOL_SOCKET,SO_RCVTIMEO,(const char *)&iTime,sizeof(DWORD));
    Timed = GetTickCount()+_t;
    while(Timed > GetTickCount())
    {
     Idx = recv(s,(char*)_b,len<<4,0);
     //Idx = recv(s,(char*)_b,len,0);
     if(Idx >= len)
			break;
    //Sleep(15UL);
    Sleep(0UL);
    }

    if(len > Idx)
    {
        if((ErrConnect++)>4)
        {
            if(s!=INVALID_SOCKET)
                tcp_close(s,TRUE);
            ErrConnect=1;
            s = INVALID_SOCKET;
        }
    }
    else
        ErrConnect=0;
//    isBlock=0UL;
//    ioctlsocket(s, FIONBIO, &isBlock); // block socket
    return Idx;
}

int _find_chr(PBYTE Buff, CHAR chr)
{
PBYTE len;
return ((len  = (PBYTE)strchr((const char *)Buff,(int) chr))!=NULL) ? (int)(len-Buff)+1 : 0;
}

int _get_digit(PBYTE Buff, int len)
{
int i,cc=0,rc=1;
BOOL find=FALSE;
PBYTE tmp;

if(!Buff[0])
    return 0;

tmp = (PBYTE) malloc_w(0x100);
   for(i=0;i<len;i++)
        {
        if((!i) && (Buff[i] <0x21))
                continue;

        if((Buff[i]=='-') || (Buff[i]=='.') || (isdigit(Buff[i])))
            {
             tmp[cc++]=Buff[i];
             tmp[cc]=0;
            }
        else if(i && isdigit(Buff[i-1]) && (!isdigit(Buff[i])))
			{
			if(cc > 2)
			    rc = (Buff[i]=='M')^1;
		    lstrcpy((LPSTR)Buff,(LPCSTR)tmp);
            break;
			}
        }
free_w(tmp);
return rc;
}

long _str2long(PBYTE Buffer)
{
long rc=0, pw=10,sign=1;
int i, len;
char cc;
len = strlen(Buffer);

    for(i=0;i<len;i++)
        {
        cc=Buffer[i];
        if(cc == '-')
            {
            sign*=-1L;
            continue;
            }
        if(isdigit(cc))
            {
            rc*=pw;
            rc+=(cc-0x30);
            }
        }
return (rc*sign);
}

int _set_zero_str(PBYTE Buff)
{
int i=0,rc=0;
int len;
        len=lstrlen((const char *) Buff);
        for(i=0;i<len;i++)
            if(Buff[i]==0x20)
                {
                Buff[i]+=0x10;
                rc++;
                }
return rc;
}
int _copy_weight_lock(PCHAR WE, PCHAR src, int sz)
{
int rc=0,iz=0,i=0;
	if((src[0]=='-'))// || (src[0]=='+'))
		{
		InterlockedExchange8((volatile PCHAR) &(WE[i++]),src[0]);
		rc=1;
		}
	for(i;i<sz;i++)
		{
		iz=1;
		InterlockedExchange8((volatile PCHAR) &(WE[rc++]),src[i]);
		InterlockedExchange8((volatile PCHAR) &(WE[rc]),0);
		}
return rc;
}

void _prnData(LPSTR s)
{
int i, len;
    len=lstrlen((LPCSTR) s);
    printf("len:%02u:",len);
    for(i=0;i<len;i++)
    {
    if(s[i] > 0x1F)
        printf("%c",(s[i]==0x20) ? '$' : s[i]);
    else
        printf("|%02X|",s[i]);
    }
    printf("\n");
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    DWORD ttick,summ=0UL,isNow;
    CHAR ipaddr[0x10]; // ����� ����
    UINT16 cmdport;    //���� ��� ������ ������ �� ������ WVK2000
    UINT16 dataport;  // ���� ��� ��������� ������ � WVK2000
    DWORD timeout; //������� �������� ������ � ����� � �������������
    DWORD cmdtimeout; //������� ������ ������ �� WVK2000 � �������������
	DWORD limit; // ��� (��) ��� ���������� �������� ���� ��������� ������.
    BYTE num;   //����� ������������ �����
    BYTE vtnum; //����� ������� WVK2000 ��� ������ �� �������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������
    SOCKET recvsocket=INVALID_SOCKET; //����� ��� ������������� �����

    int cc,i=0,ErrorLink=0,len=18;
	BYTE pktlen;
    FileMap fm;
	pCRANEINFO cri;
    long weg_1,weg_2;
    PBYTE dataExchange;
    CHAR cfg[0x40];
    BYTE Buffer[0x400];
    BYTE Data[0x400];
	HANDLE isRun;
    DWORD timeReceive,tt;
	BOOL bStatus;

if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
 		return FALSE;


cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&pktlen,&debug, &quit,&limit,cfg);
debug=0;

wsprintf((LPSTR)ReqString,"Global\\PV24pol%03d",num);
if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
		wsprintf(ReqString,"Debug PV24pol Request Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) ReqString );
        printf("��������� PV24pol -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

if(debug)
	printf("Mutex %s is open\n",ReqString);

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
cri=(pCRANEINFO)fm.dataPtr;

if(debug)
	printf("Exchange Object is open:%p\n",dataExchange);

if(cri->ID!=num)
	InterlockedExchange16((volatile SHORT *) &cri->ID,(WORD)num);


isNow=time(NULL);
InterlockedExchange((volatile LONG *) &cri->Flags,0UL);
while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param((PCHAR) ipaddr,&cmdport,&dataport,&timeout, &cmdtimeout, &num, &vtnum,&pktlen,&debug,&quit,&limit,cfg);
            if(cc & 0x2){
                if(debug){
                    sprintf(ReqString,"Debug PV24 Request Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) ReqString );
                    printf("PV24 �����:%s PV24 ���� ������:%u PV24 ��������� ����:%u\n",ipaddr,dataport,cmdport);
                }
                else
					_closeConsole();
            }
        }
    i=0;
    timeReceive=ttick=timeout;
	memset((PVOID) Buffer,0x0,(size_t)0x100);
    if(recvsocket==INVALID_SOCKET)
        recvsocket = tcp_connect((LPSTR) ipaddr, (UINT) dataport,&cc);

    cc = 0;
    if(recvsocket!=INVALID_SOCKET)
    {
        tt = GetTickCount();
        memset((PVOID)Buffer,0,(size_t)(pktlen<<4));
        i = tcp_receive(recvsocket,(PBYTE) Buffer,(int) pktlen,(DWORD)timeout);
        tt = GetTickCount() - tt;
    }

//        tt = UDP_Receive_Timed(dataport,pktlen, (PVOID) Buffer, timeout,(PWORD) &i);
            if(i && (tt < timeout))
                {
                if((Buffer[i-1]=='\r')||(Buffer[i-1]==0x3) || (Buffer[i-1]=='\n'))
                    {
                    if(Buffer[i-1]==3)
                        cc = 3;
                    else
                        cc =2;
                    }
                else
                    {
                    i=0;
                    _setFlags(&cri->Flags, LOSTWE, (BOOL) ((++ErrorLink) > 10));
                    if(ErrorLink > 10)
                        {
                        tcp_close(recvsocket,TRUE);
                        recvsocket=INVALID_SOCKET;
                        //Sleep(1000UL);
                        }
                    continue;
                    }
                    // ���� ����� �� ������������� ������� ���������
                timeReceive=timeout-tt;
                ttick=tt;
//                if(debug)
//                    printf("cc:%u\tBuffer:%c\n",cc,Buffer[i-3]);
                }
if(i)
    lstrcpy((LPSTR) Data,(LPCSTR) Buffer);
//���������� ���� ���������� ����� � WE
_setFlags(&cri->Flags, LOSTWE, (BOOL) (ErrorLink > 7));
if((cc = _find_chr((PBYTE)Buffer,'.'))>5)
   {
    bStatus = (isdigit(Buffer[cc-2]) && isdigit(Buffer[cc]));
    cc = _find_chr((PBYTE)Buffer,'U');
    cc += _find_chr((PBYTE)Buffer,'G');
    cc += _find_chr((PBYTE)Buffer,'N');
    cc += _find_chr((PBYTE)Buffer,'M');
	cc += _find_chr((PBYTE)Buffer,'E');
    bStatus &= (cc>5);
   }
else
    bStatus=FALSE;

 if((i < 6) || (!bStatus))
	{
    if((++ErrorLink) > 8)
        {
        tcp_close(recvsocket,TRUE);
        recvsocket=INVALID_SOCKET;
        ErrorLink=8;
        }
    Sleep(100UL);
    i=0;
	}
 else{
        ErrorLink=0;
        if(ttick<timeout)
            {
            // ������� ������ ������� �� ������� �� �������� �������.
            cc =  _get_digit((PBYTE) Buffer,i);
            cri->Weight_1=_str2long(Buffer);

//_____________________________________________
// ������������� ���� No Motion
            _setFlags(&cri->Flags, 0, (BOOL) cc);

// ����������� ������ � WE � ����� �������� �� �����
            lstrcpy((LPSTR)cri->Tablo,(LPCSTR)Buffer);

            cc = _copy_weight_lock((PCHAR)cri->WE, (PCHAR) Buffer, (int) lstrlen(Buffer));
    	}
    }
    if(debug&& ((time(NULL)-isNow)>=2))
        {
            isNow=time(NULL);
            printf("%lu|����:%02u|�������:%04lu -> %04lu|�����:%03u|����:%ld|Fl:0x%lX|%s;%d;",isNow,num,timeReceive,tt,i,cri->Weight_1,cri->Flags,cri->WE,(int)cri->Flags&1);
            _prnData((LPSTR)Data);
        timeReceive=timeout;
        }
    Sleep(cmdtimeout);
}

_setFlags(&cri->Flags, LOSTWE, TRUE);

if(!debug)
		_closeConsole();

	tcp_close(recvsocket,FALSE);
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // PV24POL_C
