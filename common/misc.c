#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef MISC_C
#define MISC_C

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <sddl.h>
#include "..\inc\scale.h"

#define BUF_SIZE 0x1000
#define HEIGHT 30
#define WIDTH	114
#define DIO 32

#define BSIZE 0x1000
#define USER TEXT("USERNAME")
#define COMPUTER TEXT("COMPUTERNAME")

//#define GLOBAL


#ifdef GLOBAL
PCHAR szName="Global\\ExchangeFileMappingObject";
#else
PCHAR szName="Local\\ExchangeFileMappingObject";
#endif

CHAR _Buffer[0x1000];
PCHAR szFile="Terminate.m";

PVOID _createExchangeObj(int ScaleNum, pFileMap fm)
{
SECURITY_ATTRIBUTES attributes;

ZeroMemory(&attributes, sizeof(attributes));
attributes.nLength = sizeof(attributes);
ConvertStringSecurityDescriptorToSecurityDescriptorA(
            "D:P(A;OICI;GA;;;SY)(A;OICI;GA;;;BA)(A;OICI;GWGR;;;IU)",
            SDDL_REVISION_1,
            &attributes.lpSecurityDescriptor,
            NULL);

wsprintf(_Buffer,"%s_%u",szName,ScaleNum);
fm->hMapping=OpenFileMapping(FILE_MAP_ALL_ACCESS,TRUE,_Buffer);
if(ScaleNum>1000)
	{
	printf("OpenFileMapping is open:%p\t%lx\t%s\n",fm->hMapping,GetLastError(),_Buffer);
	Sleep(5000UL);
	}


if(fm->hMapping==NULL)
	fm->hMapping=CreateFileMapping(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,BUF_SIZE,_Buffer);
//	fm->hMapping=CreateFileMapping(INVALID_HANDLE_VALUE,&attributes,PAGE_EXECUTE_READWRITE,0,BUF_SIZE,_Buffer);

if(ScaleNum>1000)
	{
	printf("CreateFileMapping is open:%p\t%lx\t%s\n",fm->hMapping,GetLastError(),_Buffer);
	Sleep(5000UL);
	}

if(fm->hMapping==NULL)
	return NULL;

fm->dataPtr = (LPBYTE) MapViewOfFile(fm->hMapping,FILE_MAP_ALL_ACCESS,0,0,BUF_SIZE);

if(ScaleNum>1000)
	{
	printf("MapViewOfFile is open:%p\t%lx\t%s\n",fm->hMapping,GetLastError(),_Buffer);
	Sleep(5000UL);
	}

if(fm->dataPtr == NULL)
   {
      CloseHandle(fm->hMapping);
      return NULL;
   }
return (PVOID) fm->dataPtr;
}

void _destroyExchangeObj(int ScaleNum, pFileMap fm)
{
UnmapViewOfFile(fm->dataPtr);
CloseHandle(fm->hMapping);
}

BOOL _chkConfig(LPSTR mask)
{
static FILETIME ft={0};
FILETIME ftWrite;
WIN32_FIND_DATA findData;
HANDLE f;

f=FindFirstFile(mask,&findData);
FindClose(f);

if(( f = CreateFile((LPCTSTR) findData.cFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL))==INVALID_HANDLE_VALUE)
		return FALSE;
	if(!GetFileTime(f, NULL, NULL, &ftWrite))
		{
		CloseHandle(f);
		return FALSE;
		}
if((ftWrite.dwLowDateTime == ft.dwLowDateTime) && (ftWrite.dwHighDateTime == ft.dwHighDateTime))
		{
		CloseHandle(f);
		return FALSE;
		}

ft.dwLowDateTime=ftWrite.dwLowDateTime;
ft.dwHighDateTime=ftWrite.dwHighDateTime;
CloseHandle(f);
return TRUE;
}

int _copy_str_until(PCHAR dst,PCHAR src, CHAR chr)
{
 int i, rc=0;
        for(i=0;i<64; i++)
        {
            if((src[i]==chr) || (!src[i]))
            {
                dst[i]=0;
                rc=i;
                break;
            }
         dst[i]=src[i];
        }
return rc;
}

int _alltrim(PCHAR dst,PCHAR src)
{
 int i,j=0,len;
len=lstrlen(src);
        for(i=0;i<len; i++)
        {
            if(src[i]==' ')
				continue;
         dst[j++]=src[i];
        }
dst[j]=0;
return j;
}

int _replace_str(PCHAR src, CHAR c, CHAR r)
{
int rc=0,i,len;
len=lstrlen(src);
	for(i=0;i<len;i++)
		if(src[i]==c)
			{
			src[i]=r;
			rc++;
			}
return rc;
}

int  _find_cmd_variable(PCHAR data, PCHAR var, PCHAR value)
{
int Idx=0,rc=0,sLen;
CHAR test[0x20];
if(*((char *)data))
	{
//--���� ����, �� ������ �� ���� �� ��������� ������--
	if(*((char *)data)=='/' || *((char *)data)=='-' || *((char *)data)=='\\')
		Idx++;
    sLen=lstrlen((LPCSTR) var);
    lstrcpyn((LPSTR) test,(LPCSTR) data+Idx,sLen+1);
	if(!lstrcmpi((LPCSTR) (test), (LPCSTR) var))
        {
        Idx+=sLen;
        if(data[Idx]=='=')
            rc=_copy_str_until(value,data+Idx+1,' ');
        }
	}
return rc;
}

BYTE R_TRIG32(BYTE Ch,BYTE iCLK)
{
static BYTE lM[DIO]={0};
BYTE lRc;
if(Ch >=DIO)
	Ch=DIO-1;
lRc= iCLK && !lM[Ch];
lM[Ch]=iCLK;
return lRc;
}

BYTE F_TRIG32(BYTE Ch,BYTE iCLK)
{
static BYTE lM[DIO]={0};
BYTE lRc;
if(Ch >=DIO)
	Ch=DIO-1;
lRc= !iCLK && !lM[Ch];
lM[Ch]=!iCLK;
return lRc;
}

INT  _DeleteLog(int mon,int sNum)
{
SYSTEMTIME st;
WIN32_FIND_DATA ffd;
HANDLE hFind;
INT rc=0;
CHAR mask[0x20];
CHAR path[0x40];

GetSystemTime(&st);
if((st.wDay!=27) && (st.wHour!=12) && (st.wMinute!=1))
		return rc;

if(st.wMonth<=mon)
	st.wMonth+=12;
st.wMonth-=mon;
wsprintf(mask,"%02d/\?\?%02d*.\?\?\?",sNum,st.wMonth);
   hFind = FindFirstFile(mask, &ffd);
   if (hFind == INVALID_HANDLE_VALUE)
      return rc;
do {
	wsprintf(path,"%02d/%s",sNum,ffd.cFileName);
	DeleteFile((LPCTSTR)path);
	rc++;
}while (FindNextFile(hFind, &ffd) != 0);
FindClose(hFind);
return rc;
}

HANDLE _chkIsRun(LPSTR AppName)
{
    HANDLE hMutex;
    DWORD err;
    hMutex = CreateMutex(NULL,FALSE,(const PCHAR) AppName);
    err = GetLastError();
    if(err)
        return NULL;
    else
        return hMutex;
}

void _openConsole(LPSTR head)
{
HANDLE hsoh;
CONSOLE_FONT_INFOEX cfi;
SMALL_RECT conSize={WIDTH-1,HEIGHT-1};
COORD bufSize={WIDTH,HEIGHT};

                    AllocConsole();
                    freopen("CONOUT$","w",stdout);
                    SetConsoleOutputCP(1251);
                    SetConsoleTitle(head);
					hsoh=GetStdHandle(STD_OUTPUT_HANDLE);
                    SetConsoleTextAttribute(hsoh, FOREGROUND_GREEN | /*FOREGROUND_BLUE |*/ FOREGROUND_INTENSITY /*| FOREGROUND_RED*/);
			        SetConsoleWindowInfo(hsoh,TRUE,&conSize);
					SetConsoleScreenBufferSize(hsoh,bufSize);
					DeleteMenu(GetSystemMenu(GetConsoleWindow(),FALSE),SC_CLOSE,MF_BYCOMMAND);
					cfi.cbSize = sizeof(cfi);
				    cfi.nFont = 0;
				    cfi.dwFontSize.X = 10;
				    cfi.dwFontSize.Y = 26;
				    cfi.FontFamily = FF_DONTCARE;
				    cfi.FontWeight = FW_BOLD;
					lstrcpy((LPSTR) cfi.FaceName, "Lucida Console");
//					lstrcpy((LPSTR) cfi.FaceName, "Verdana");
					SetCurrentConsoleFontEx(hsoh, FALSE, &cfi);

}

void _closeConsole(void)
{
					fclose(stdout);
                    FreeConsole();

}

BYTE InterlockedExchange8(CHAR volatile *Target, CHAR Value)
{
BYTE old;
old= *Target;
/*
if(Value)
	return((BYTE)_InterlockedOr8(Target,Value));
else
	return((BYTE)_InterlockedAnd8(Target,Value));

__asm__ ( "lock xchgb %bl, (%rax)" \
		:
		: "b" (Value), "a" (Target)
		: "%rbx"
	);
*/
__sync_lock_test_and_set(Target,Value);
return old;
}

SHORT InterlockedExchange16(SHORT volatile *Target, SHORT Value)
{
BYTE old;
old= *Target;
/*
if(Value)
	return((BYTE)InterlockedOr16(Target,Value));
else
	return((BYTE)InterlockedAnd16(Target,Value));
*/
__sync_lock_test_and_set(Target,Value);
return old;
}

void WriteLogWeb(LPSTR Buf, int scaleNum, BOOL en)
{
struct tm *nt;
time_t lt;
FILE * logFile;
// ������� ���� ������ ���������
if(!en) return;

lt =  time(NULL);
nt = localtime(&lt);

wsprintf(_Buffer,"%02d/%02d%02d%04d.web",scaleNum,nt->tm_mday,nt->tm_mon+1,nt->tm_year+1900);

	logFile=fopen(_Buffer,"a+");
	fprintf(logFile,"%02d.%02d.%04d %02d:%02d:%02d:\t",nt->tm_mday,nt->tm_mon+1,nt->tm_year+1900,nt->tm_hour,nt->tm_min,nt->tm_sec);
	fprintf(logFile,"����:%d\t%s\n",scaleNum,Buf);

fflush(logFile);
fclose(logFile);
}

BOOL isNoLogin(LPSTR _envstr)
{
BOOL rc=FALSE;
DWORD dwRet;

dwRet = GetEnvironmentVariable((LPCTSTR) USER, (LPTSTR) _envstr, (DWORD)BSIZE);
if(_envstr[dwRet-1]!='$')
	return rc;
_envstr[dwRet-1]=0;
dwRet = GetEnvironmentVariable((LPCTSTR) COMPUTER, (LPTSTR) _Buffer, (DWORD)BSIZE);
if(!lstrcmp(_envstr,_Buffer))
	return TRUE;
return rc;
}

BOOL _setFlags(volatile PDWORD flag, BYTE Num, BOOL val)
{
volatile DWORD tmpFlag;
DWORD dwBit=1;

dwBit <<= Num;
InterlockedExchange((volatile LONG *) &tmpFlag,*flag);
//tmpFlag = (*flag);
if(val)
	InterlockedExchange((volatile LONG *) flag,(tmpFlag | dwBit));
else
	InterlockedExchange((volatile LONG *) flag,(tmpFlag & (~dwBit)));
return val;
}

BOOL _getFlags(volatile PDWORD flag, BYTE Num)
{
volatile DWORD tmpFlag;
//InterlockedExchange((volatile LONG *) &tmpFlag,flag);
tmpFlag = (*flag);
tmpFlag>>=Num;
return (tmpFlag & 1);
}

UINT16 hex2dec(PCHAR hexVal)
{
int len, i,cc;
UINT16 dec_val = 0;

len = lstrlen(hexVal);
    // Extracting characters as digits from last character
    for (i=0; i<len; i++)
    {
    if(!isxdigit(hexVal[i]))
        continue;
	dec_val<<=4;
	cc = tolower(hexVal[i])-0x30;
	if(cc < 0)break;
        if(cc>=0 && cc<=9)
            dec_val += cc;
         else{
            cc-=0x27;
            dec_val += cc;
         }
    if(cc > 0xF)break;
    }
return dec_val;
}

INT16 _getDI(PBYTE buf, int len)
{
static BYTE buffer[0x20]={0};
static int Idx=0,state=0;
INT16 i;
INT16 rc=-1;
	for(i=0;i<len;i++)
	{
		if(isxdigit((int)buf[i]) && (state))
			buffer[Idx++]=buf[i];
		if(isxdigit((int)buf[i]) && (!state))
		{
			Idx=0;
			state=1;
			buffer[Idx++]=buf[i];
		}
		if((!isxdigit((int)buf[i])) && state)
			{
			buffer[Idx]=0;
			rc=hex2dec((PCHAR) buffer);
			Idx=state=0;
			}
	}
return rc;
}
#endif // MISC_C
