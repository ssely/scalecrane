#include <winsock2.h>
#include <windows.h>
#include "..\inc\scale.h"

#pragma GCC diagnostic ignored "-Wunused-variable"

BYTE init=0;
SOCKET SendingSocket=INVALID_SOCKET,ReceiveSocket=INVALID_SOCKET;

int UDP_Init(void)
{
    WSADATA wsaData;
    if(init)
        return 0;
    if( WSAStartup(MAKEWORD(2,2), &wsaData) != 0)
    {
        WSACleanup();
        return -1;
    }


    init=1;
    return (int) init;
}

int UDP_Close(void)
{
    if(SendingSocket!=INVALID_SOCKET)
        closesocket(SendingSocket);
    if(ReceiveSocket!=INVALID_SOCKET)
        closesocket(ReceiveSocket);
    SendingSocket=INVALID_SOCKET;
    ReceiveSocket=INVALID_SOCKET;
    WSACleanup();
    init=0;
    return init;
}

int UDP_OpenSend(void)
{
    int rc = -1;
    if(!init)
        return rc;

    SendingSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (SendingSocket != INVALID_SOCKET)
        rc=1;
    else
        UDP_Close();
    return rc;
}

int UDP_OpenReceive(void)
{
    int rc = -1;
	DWORD rcvBuf=0x10000;
    if(!init)
        return rc;

    ReceiveSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (ReceiveSocket != INVALID_SOCKET)
		{
		setsockopt(ReceiveSocket,SOL_SOCKET,SO_RCVBUF,(char *)&rcvBuf,sizeof(rcvBuf));
        rc=1;
		}
    else
        UDP_Close();
    return rc;
}

int UDP_Send(PCHAR addr,UINT16 port, PVOID SendBuf, int Buflen)
{
    SOCKADDR_IN ReceiverAddr, SrcInfo;
    int rc=-1;
    if(!init)
        return rc;
    ReceiverAddr.sin_family = AF_INET;
    ReceiverAddr.sin_port = htons(port);
    ReceiverAddr.sin_addr.s_addr = inet_addr(addr);
    rc=sendto(SendingSocket, SendBuf, Buflen,0,(SOCKADDR *)&ReceiverAddr, sizeof(ReceiverAddr));
    return rc;
}

int recvfromTimeOutUDP(long sec, long usec)
{
    // Return value:
    // -1: error occurred
    // 0: timed out
    // > 0: data ready to be read

    int rc=0;
    struct timeval timeout;
    struct fd_set fds;
    if(ReceiveSocket == INVALID_SOCKET)
        return -1;
    timeout.tv_sec = sec;
    timeout.tv_usec = usec;
    FD_ZERO(&fds);
    FD_SET(ReceiveSocket, &fds);
    rc = select(0, &fds, 0, 0, &timeout);
    return rc;
}

int UDP_Receive(UINT16 Port,int len, PVOID buf)
{
    static BYTE isBind=0;
    int rc = -1,WaitByte;
    SOCKADDR_IN ReceiverAddr,SenderAddr;
    int SenderAddrSize;
    long LongE;
    DWORD DwordE;

    if(!init)
    {
        isBind=0;
        return rc;
    }

    ReceiverAddr.sin_family = AF_INET;
    ReceiverAddr.sin_port = htons(Port);
    ReceiverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(!isBind)
    {
        if(bind(ReceiveSocket, (SOCKADDR *)&ReceiverAddr, sizeof(ReceiverAddr)) == SOCKET_ERROR)
            return rc;
        else
            isBind=1;
    }
    if(isBind)
    {
        if((WaitByte = recvfromTimeOutUDP(0, 1UL))<0)
        {
            isBind=0;
            return rc;
        }
        SenderAddrSize = sizeof(SenderAddr);
        if(WaitByte >0 )
        {
            rc = recvfrom(ReceiveSocket,buf,len,0,(SOCKADDR *)&SenderAddr,&SenderAddrSize);
            if(rc>=len)
                return rc;

        }
        if(!WaitByte)
            rc=WaitByte;
    }
    return rc;
}

DWORD UDP_Receive_Timed(UINT16 Port,int len, PVOID buf, DWORD tout,PWORD wlen)
{
    DWORD rc=-1L,err;
    int cc,sum=0;
    DWORD theEnd;
	PBYTE tmpBuff;

    memset((PVOID)buf,0,len<<1);

    *wlen = 0;
    theEnd = GetTickCount()+tout;

    tmpBuff=(PBYTE) malloc_w(len<<2);

        while(theEnd >= GetTickCount())
        {
            cc=0;
            cc=UDP_Receive(Port,len-sum,tmpBuff+sum);
            if(cc > 0)
            {
            if(tmpBuff[sum])
                    sum+=cc;
            rc = theEnd-GetTickCount();
			if(sum >=len)
                {
                memmove((PVOID)buf,(const PVOID)tmpBuff,(size_t)len);
                break;
                }
            }
        }
*wlen = (WORD) sum;
free_w(tmpBuff);
return rc;
}
