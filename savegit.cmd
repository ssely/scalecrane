@echo off
setlocal disabledelayedexpansion

set DAY=%date:~0,2%
set MON=%date:~3,2%
set YEAR=%date:~6,4%
set HOUR=%time:~0,2%
set MIN=%time:~0,2%
git commit -am "Created at %date% %time%"
git push