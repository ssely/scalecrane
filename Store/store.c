#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <winsock2.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\scale.h"
#include "..\inc\sqlite3.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"

#define ENTER 1
#define LEFT 2
#define RIGHT 3
#define UP 4
#define DOWN 5
#define LOSTCONNECT 7
#define MIRRORENTER (ENTER+0x10)

CHAR WeString[0x100];


int _get_param(PBYTE number,PCHAR path,PCHAR shema,PBYTE keyBit,BOOL* debug, BOOL* quit,PCHAR IniFile)
{
static BOOL isFirst=FALSE;
PCHAR tmp;  //�������� ����������
PCHAR CurDir;
int rc=0,cc;

CurDir=(PCHAR) malloc_w(0x200);
GetCurrentDirectory(0x100,CurDir);
//SetDllDirectory((LPCSTR)CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
tmp=(PCHAR) malloc_w(0x100);
GetPrivateProfileString("sql","dbpath","store.db",tmp,0xFF,(LPCSTR)CurDir);
_alltrim(tmp,tmp);
_copy_str_until(tmp,tmp,';');
    if(lstrcmp((LPCSTR)tmp,(LPCSTR)path))
    {
        lstrcpy((LPSTR)path,(LPCSTR)tmp);
        rc|=1;
    }
GetPrivateProfileString("sql","dbshema","",tmp,0xFF,(LPCSTR)CurDir);
//_copy_str_until(tmp,tmp,';');
    if(lstrcmp((LPCSTR)tmp,(LPCSTR)shema))
    {
        lstrcpy((LPSTR)shema,(LPCSTR)tmp);
        rc|=1;
    }

*number=(BYTE)GetPrivateProfileInt("scale","num",0,(LPCSTR)CurDir);
*keyBit=(BYTE)GetPrivateProfileInt("sql","savebit",0,(LPCSTR)CurDir);

cc=GetPrivateProfileInt("sql","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }

*quit=(BOOL)GetPrivateProfileInt("sql","exit",0,(LPCSTR)CurDir);
if(!isFirst)
{
	wsprintf(WeString,"Debug SQL Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) WeString );
	printf("������������ SQL �� �����:\t%s\n",(PCHAR)CurDir);
	printf("���� � ��:\t\t\t%s\n",(PCHAR)path);
	printf("����� �������:\t\t\t%s\n",(PCHAR)shema);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	printf("��� ������ � ��:\t\t\t%u\n",*keyBit);
	Sleep(5000UL);
	_closeConsole();
}
isFirst=*debug;
cc=0;
free_w(tmp);
free_w(CurDir);
return rc;
}

INT8 _del_zero_str(PCHAR dst, PCHAR src)
{
INT8 rc=0,i=0,delz=0;
while(src[i])
    {
    if((src[i]=='-') || (src[i]=='+'))
        i++;
    if((!delz) && (src[i]!='0'))
            delz=1;
    if((!delz) && (src[i]=='0'))
        i++;
    if(delz)
        dst[rc++]=src[i++];
    dst[rc]=0;
    }
return rc;
}

INT16 _get_month(INT16 m)
{
INT16 rc;
	if(m < 3)
		rc=12+m;
	else
		rc=m;
return(rc-2);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
    time_t isNow,isReset;
    PCHAR dbpath; // ���� � ����� ��
    PCHAR dbshema; // ����� �������
    PCHAR SqlStr;

    BYTE num,entr; // ����� �����
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������
	BOOL del=FALSE; // �������� ������ �������
    FileMap fm;
    volatile pCRANEINFO pCri;
    PBYTE dataExchange;
    PCHAR cfg;
	HANDLE isRun;
	int cc,i,col,Idx;
	PCHAR sqlName,colName;
	sqlite3 *sl;
	sqlite3_stmt *res;
	struct tm *nt;
	FILE * fs;

cfg = (PCHAR) malloc_w(0x100);
if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf",(PCHAR) cfg)))
{
    free_w(cfg);
    return FALSE;
}


dbpath = (PCHAR) malloc_w(0x100);
dbshema = (PCHAR) malloc_w(0x100);
SqlStr = (PCHAR) malloc_w(0x400);

cc=_get_param(&num,dbpath,dbshema,&entr,&debug,&quit,(PCHAR)cfg);

debug=0;
wsprintf((LPSTR)SqlStr,"Global\\SQLSTR%03d",num);
    if((isRun = _chkIsRun((LPSTR)SqlStr))==NULL)
    {
		wsprintf(SqlStr,"Debug SQL Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) SqlStr );
        printf("��������� Store -conf=%s  ��� ��������. �������...",cfg);
        free_w(SqlStr);
        free_w(dbshema);
        free_w(dbpath);
    Sleep(5000UL);
    _closeConsole();
    return FALSE;
    }

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
pCri=(pCRANEINFO)fm.dataPtr;

isReset=isNow=time(NULL);

    if((cc = sqlite3_open_v2((const char *)dbpath,&sl,SQLITE_OPEN_READWRITE,NULL))!=SQLITE_OK)
        {
        if((cc = sqlite3_open_v2((const char *)dbpath,&sl,SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE,NULL))==SQLITE_OK)
            {
                cc = sqlite3_exec(sl,dbshema,NULL,NULL,NULL);
                cc = sqlite3_close_v2(sl);
            }

        }
    else{
        cc = sqlite3_prepare_v2(sl,"SELECT * FROM ladle ORDER BY utc DESC LIMIT 4",-1,&res,0);
        res=NULL;
        res = sqlite3_next_stmt(sl, NULL);
        col = sqlite3_column_count(res);
        Idx=0;
        while((cc = sqlite3_step(res))==SQLITE_ROW)
            {
            for(i=0;i<col;i++)
                {
                sqlName = (PCHAR)sqlite3_column_text(res, i);
                switch(i)
                    {
                    case 0:
							 if(sqlName[4]=='-')
                                wsprintf((LPSTR)pCri->Store[Idx],"%2.2s.%2.2s.%4.4s %8.8s",sqlName+8,sqlName+5,sqlName,sqlName+11);
                            else
                                wsprintf((LPSTR)pCri->Store[Idx],"%s",sqlName);
                    break;
                    default:lstrcat((LPSTR)pCri->Store[Idx],(LPCSTR)";");
                            lstrcat((LPSTR)pCri->Store[Idx],(LPCSTR)sqlName);
                    }
//                colName = (PCHAR)sqlite3_column_name(res, i);
                }
            Idx++;
            }
        cc = sqlite3_close_v2(sl);
    }
if(cc!=SQLITE_OK)
    {
        if(debug)
            {
            printf("%lu|����:%02u|���������� ������� DB:%s| �������...\n",time(NULL),num,dbpath);
            Sleep(2000UL);
            quit=1;
            }

    }
    while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param(&num,dbpath,dbshema,&entr,&debug,&quit,(PCHAR)cfg);
            if(cc & 0x2){
                if(debug){
					wsprintf(SqlStr,"Debug SQL Console: %s\n",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) WeString );
                    printf("SQL DB:%s\n",dbpath);
                }
                else
					_closeConsole();
            }
        }
//    lstrcpy(pCri->WE,"12.38");

    i = _getFlags(&pCri->Flags,entr);
	if(R_TRIG32(ENTER,_getFlags(&pCri->Flags,entr)) && (!_getFlags(&pCri->Flags,entr+0x10)))
//	if((i) && (!_getFlags(&pCri->Flags,entr+0x10)))
			{
            i = _getFlags(&pCri->Flags,entr+0x10);
			_setFlags(&(pCri->Flags), entr+0x10, 1); // ������������� ���� ����������
			i = _getFlags(&pCri->Flags,entr+0x10);
			isNow=time(NULL);
			nt = localtime(&isNow);
//            cc = _del_zero_str((PCHAR) WeString,(PCHAR)pCri->WE);
  			lstrcpy((LPSTR)WeString,(LPCSTR)pCri->WE);
			sprintf(SqlStr,"INSERT INTO ladle (dt,utc,id_scale,id_station,month,dir,number,we_move,weight) VALUES ('%04u-%02u-%02u %02u:%02u:%02u','%lu',%u,%u,%u,%u,%u,%u,'%s')",
												nt->tm_year+1900,nt->tm_mon+1,nt->tm_mday,nt->tm_hour,nt->tm_min,nt->tm_sec,(DWORD)isNow,num,(WORD)pCri->ID_Station,nt->tm_mon+1,
                                                (WORD)pCri->Operation,(WORD)pCri->ID_Scoop,(UINT16)_getFlags(&pCri->Flags, 0)^1,WeString);
			if(debug)
				printf("%s|FL:0x%lX\n",SqlStr,pCri->Flags);
            pCri->Flags=pCri->Flags;
            if((cc = sqlite3_open_v2((const char *)dbpath,&sl,SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE,NULL))==SQLITE_OK)
                {
                cc |= sqlite3_exec(sl,SqlStr,NULL,NULL,NULL);
                cc |= sqlite3_close_v2(sl);
				if(debug)
					printf("Error code:%d\tError msg:%s\n",cc,sqlite3_errstr(cc));
				if(!cc)
					{
					for(i=4;i>0;i--)
						lstrcpy(pCri->Store[i],pCri->Store[i-1]);
					sprintf(pCri->Store[0],"%02u.%02u.%04u %02u:%02u:%02u;%lu;%u;%u;%u;%u;%u;%u;%s",
											nt->tm_mday,nt->tm_mon+1,nt->tm_year+1900,nt->tm_hour,nt->tm_min,nt->tm_sec,(DWORD)isNow,num,(WORD)pCri->ID_Station,nt->tm_mon+1,(WORD)pCri->Operation,(WORD)pCri->ID_Scoop,(UINT16)_getFlags(&pCri->Flags, 0)^1,WeString);
					if(debug)
						printf("Store:%s\n",pCri->Store[0]);
// ������ ������� ��� ������� � Web �������
                wsprintf(SqlStr,".\\%02d",num);
                if((GetFileAttributes(SqlStr)==INVALID_FILE_ATTRIBUTES))
                        CreateDirectory(SqlStr, NULL);
                wsprintf(SqlStr,".\\%02d\\%s",num,"store.txt");
                if((fs=fopen(SqlStr,"w+t"))!=NULL)
                    {
                fprintf(fs,"dt;utc;id_scale;id_station;month;dir;number;we_move;weight\n");
                    for(i=0;i<5;i++)
						{
						if(!pCri->Store[i][0])
							break;
                        fprintf(fs,"%s\n",pCri->Store[i]);
						}
                    fflush(fs);
                    fclose(fs);
                    }
                }
//				Sleep(30000UL);
				if(!cc) // ���� ��� ������ ���������� ������� ����� ����������
					isReset = time(NULL);

// ������� ���� �� ���������� �������
				if((nt->tm_mday==28) && (!del))
					{
					if(sqlite3_open_v2((const char *)dbpath,&sl,SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE,NULL)==SQLITE_OK)
						{
						sprintf(SqlStr,"DELETE FROM ladle WHERE month=%u",_get_month(nt->tm_mon+1));
		                cc |= sqlite3_exec(sl,SqlStr,NULL,NULL,NULL);
						cc |= sqlite3_exec(sl,"VACUUM",NULL,NULL,NULL);
						sqlite3_close_v2(sl);
						del = (cc==0);
						}
					}
				else if(nt->tm_mday!=28)
						del=FALSE;
                }
				else if(debug)
					printf("Error code:%d\tError msg:%s\n",cc,sqlite3_errstr(cc));

				isNow=time(NULL);
//________________________________
			}
	if(((time(NULL)-isReset)>=2) && (_getFlags(&pCri->Flags, entr+0x10)))
			{
			if(debug)
	            printf("%lu|����:%02u|WE:%s|Rst:0x%lX\n",time(NULL),num,pCri->WE,(DWORD)pCri->Flags);
			_setFlags(&pCri->Flags, entr+0x10, 0);
			}

   	if((time(NULL)-isNow)>=5)
			{
			if(debug)
	            printf("%lu|����:%02u|WE:%s|Fl:0x%lX\n",time(NULL),num,pCri->WE,(DWORD)pCri->Flags);
			if(_getFlags(&pCri->Flags,entr))
				_setFlags(&pCri->Flags, entr, 0);
			isNow=time(NULL);
			}
    Sleep(100UL);
    }
if(!debug)
	_closeConsole();
    free_w(SqlStr);
    free_w(dbshema);
    free_w(dbpath);
	free_w(cfg);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
