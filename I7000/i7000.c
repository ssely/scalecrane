#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef I7000_C
#define I7000_C

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <winsock2.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\scale.h"
#include "..\inc\udp2mat.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

#define LEFT 2
#define RIGHT 3
#define UP 4
#define DOWN 5
#define LOSTCONNECT 7

CHAR DioString[0x100];


int _get_param(PCHAR ip_addr,PUINT16 dataport,PUINT16 cmdport,PINT16 moveUp, PINT16 moveDown, PINT16 moveLeft, PINT16 moveRight, PCHAR dioCmd, PDWORD tmr, PBYTE number, PBYTE reverse, BOOL* debug, BOOL* quit,PCHAR IniFile)
{
    static BOOL isFirst=FALSE;
    PCHAR ip; // ip ����� ioLogic
    PCHAR CurDir;
    int rc=0,cc;

    CurDir=(PCHAR) malloc_w(0x200);
    ip=(PCHAR) malloc_w(0x100);
    GetCurrentDirectory(0x100,CurDir);
    lstrcat((LPSTR)CurDir,"\\");
    lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
    GetPrivateProfileString("i7000","ip","127.0.0.1",ip,0xFF,(LPCSTR)CurDir);
    _alltrim(ip,ip);
    _copy_str_until(ip,ip,';');
    if(lstrcmp((LPCSTR)ip,(LPCSTR)ip_addr))
    {
        lstrcpy((LPSTR)ip_addr,(LPCSTR)ip);
        rc|=1;
    }
    GetPrivateProfileString("i7000","addr","0",ip,15,(LPCSTR)CurDir);
    _alltrim(ip,ip);
    _copy_str_until(ip,ip,';');
    lstrcpy((LPSTR) dioCmd,(LPCSTR)ip);
    lstrcat((LPSTR)dioCmd,(LPCSTR)"\r");

    *number=(BYTE)GetPrivateProfileInt("scale","num",0,(LPCSTR)CurDir);
    *dataport=(UINT16)GetPrivateProfileInt("i7000","dataport",4002,(LPCSTR)CurDir);
    *cmdport=(UINT16)GetPrivateProfileInt("i7000","cmdport",4001,(LPCSTR)CurDir);
    *moveLeft=GetPrivateProfileInt("i7000","moveLeft",-1,(LPCSTR)CurDir);
    *moveRight=GetPrivateProfileInt("i7000","moveright",-1,(LPCSTR)CurDir);
    *moveUp=GetPrivateProfileInt("i7000","moveup",-1,(LPCSTR)CurDir);
    *moveDown=GetPrivateProfileInt("i7000","movedown",-1,(LPCSTR)CurDir);
    *reverse=(BYTE)GetPrivateProfileInt("i7000","reverse",0,(LPCSTR)CurDir);
    *tmr=(DWORD)GetPrivateProfileInt("i7000","timeout",2000,(LPCSTR)CurDir);
    cc=GetPrivateProfileInt("i7000","debug",0,(LPCSTR)CurDir);

    if(*debug != (BOOL)cc)
    {
        rc|=2;
        *debug = (BOOL)cc;
    }
    *quit=(BOOL)GetPrivateProfileInt("i7000","exit",0,(LPCSTR)CurDir);

    if(!isFirst)
    {
        wsprintf(DioString,"Debug DIO Console: %s",SC_UBUNTU_FULLVERSION);
        _openConsole((LPSTR) DioString );
        printf("������������ I7000 �� �����:\t%s\n",(PCHAR)CurDir);
        printf("IP ����� I7000:\t\t\t\t%s\n",(PCHAR)ip_addr);
        printf("���� ������:\t\t\t\t%lu\n",(UINT16) *cmdport);
        printf("���� ������:\t\t\t\t%lu\n",(UINT16) *dataport);
        printf("����� �����:\t\t\t\t%03d\n",(INT) *number);
        printf("����� ����� �����:\t\t\t%d\n",(INT16) *moveLeft);
        printf("����� ����� ������:\t\t\t%d\n",(INT16) *moveRight);
        printf("����� ����� �����:\t\t\t%d\n",(INT16) *moveUp);
        printf("����� ����� ����:\t\t\t%d\n",(INT16) *moveDown);
        printf("������� ������:\t\t\t\t%s\n",(PCHAR) dioCmd);
        printf("�������� �������:\t\t\t%s\n",(reverse > 0) ? "��������" : "������");
        printf("������� ������:\t\t\t\t%lu ms\n", *tmr);
        printf("���������� �������:\t\t\t%s\n", *debug ? "���" : "����");
        Sleep(5000UL);
        if((rc&2)==0)
            _closeConsole();
    }
    isFirst=*debug;
    cc=0;
    free_w(ip);
    free_w(CurDir);
    return rc;
}

INT16 _detectModule(PCHAR name, PCHAR command, PINT16 lAnsw)
{
    INT16 rc=-1,len,cc=0;
    CHAR lname[0x10]= {0};

    cc=_copy_str_until((PCHAR) lname,name, '\r');

    if((len=lstrlen((LPCSTR)lname)) < 5)
        return rc;
//if(lname[len-1]!=0xD)
//        return rc;
//lname[len-1]=0;

    if(lname[len-1]=='D')
        lname[len-1]=0;

    if(!lstrcmp((LPCSTR)(lname+3),(LPCSTR)"7050"))
        cc=5;
    if(!lstrcmp((LPCSTR)(lname+3),(LPCSTR)"7053"))
        cc=5;
    if(!lstrcmp((LPCSTR)(lname+3),(LPCSTR)"7063"))
        cc=5;
    switch(cc)
    {
    case 0:
        break;
    case 1:
        break;
    case 2:
        break;
    case 3:
        break;
    case 4:
        break;
    case 5:
        name[0]='@';
        name[3]='\r';
        name[4]=0;
        lstrcpy((LPSTR)command,(LPCSTR)name);
        *lAnsw=6;
        rc=lstrlen(command);
        break;
    }

    return rc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
    DWORD isNow;
    CHAR ipaddr[0x10]; // ����� ����
    CHAR iCmd[0x10]= {0}; // ������� ������ ������
    CHAR Answ[0x20],Data[0x20];
    BYTE num; // ����� �����
    BYTE rvr; // �������� �������
    INT16 mUp,mDown,mLeft,mRight;    //����� ����������� �������� �����
    UINT16 dport,cport; // ����� ��� ���������� �������� �������
    DWORD timeout; // ������� ������ � �������������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������
    BOOL lostconn=1; // ���� ������ ����������
    int cc;
    INT16 mLen=-1; //����� ������� � ������
    INT16 aLen=-1; //����� ������ �� ������
    UINT16 diData,diMask=1,pData=0,i;
    FileMap fm;
    pCRANEINFO pCri;
    PBYTE dataExchange;
    CHAR cfg[0x20];
    HANDLE isRun;


    if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf", (PCHAR) cfg)))
        return FALSE;

    cc=_get_param((PCHAR)ipaddr,&dport,&cport,&mUp,&mDown,&mLeft,&mRight,iCmd,&timeout,&num,&rvr,&debug,&quit,(PCHAR)cfg);

    //debug=0;
    wsprintf((LPSTR)DioString,"Global\\I7000%03d",num);
    if((isRun = _chkIsRun((LPSTR)DioString))==NULL)
    {
        wsprintf(DioString,"Debug I7000 Console: %s",SC_UBUNTU_FULLVERSION);
        _openConsole((LPSTR) DioString );
        printf("��������� I7000 -conf=%s  ��� ��������. �������...",cfg);
        Sleep(5000UL);
//    _closeConsole();
        return FALSE;
    }

    dataExchange=(PBYTE)_createExchangeObj(num, &fm);
    pCri=(pCRANEINFO)fm.dataPtr;

    if(pCri->ID!=num)
        InterlockedExchange((volatile DWORD *) &pCri->ID,(DWORD)num);
    cc=UDP_Init();

    if(debug)
        printf("Udp Init:%d\n",cc);

    cc=UDP_OpenSend();
    if(debug)
        printf("Udp Send Init:%d\n",cc);

    cc=UDP_OpenReceive();
    if(debug)
        printf("Udp Reseive Init:%d\n",cc);

    cc=UDP_Send(ipaddr,cport, (PVOID) iCmd, mLen);
    cc=UDP_Receive_Timed(dport,10, (PVOID) Answ, timeout,(PWORD) &i);

    if(cc!=-1)
        mLen=_detectModule((PCHAR) Answ, (PCHAR) iCmd, (PINT16) &aLen);

    isNow=time(NULL);

//    _closeConsole();
_chkConfig((LPSTR) cfg);
    while(!quit)
    {
        if(_chkConfig((LPSTR) cfg))
        {
            cc=_get_param((PCHAR) ipaddr,&dport,&cport,&mUp,&mDown,&mLeft,&mRight,iCmd,&timeout,&num,&rvr,&debug,&quit,(PCHAR)cfg);
            if(cc & 0x2)
            {
                if(debug)
                {
                    sprintf(DioString,"Debug I7000 Console: %s",SC_UBUNTU_FULLVERSION);
                    _openConsole((LPSTR) DioString );
                    printf("I7000 �����:%s I7000 ��������� ����:%u\t���� ������:%u\n",ipaddr,cport,dport);
                }
                else
                    _closeConsole();
            }
        }
		if(debug)
			printf("1.Send <-> Receive : %d\n",cc);
        if(iCmd[3]=='M')
        {
            mLen=lstrlen((LPCSTR)iCmd);
            aLen=9;
        }
        cc=UDP_Send(ipaddr,cport, (PVOID) iCmd, mLen);
		if(debug)
			printf("2.Send <-> Receive : %d\n",cc);
        memset((PVOID) Answ,0x0,(size_t)0x1F);
        cc=UDP_Receive_Timed(dport,aLen, (PVOID) Answ, timeout,(PWORD) &i);
		if(debug)
			printf("3.Send <-> Receive : %d\n",cc);

        if(i==aLen)
        {
            memmove(Data,Answ,i);
            Data[i-1]=0;
        }
        else if(i > 2)
            sprintf(Data,"Error Len:%u ti:%u s:|%02X|%02X|%02X|",aLen,i,Answ[0],Answ[1],Answ[2]);

        if((++lostconn)>5)
            lostconn=5;

        if((iCmd[3]=='M') && (cc>-1))
            mLen=_detectModule((PCHAR) Answ, (PCHAR) iCmd, (PINT16) &aLen);
        else if(cc>-1)
        {
            if(Answ[0]=='>')
                diData = hex2dec(Answ);
            _setFlags(&pCri->Flags, UP, (BOOL) ((diData & (diMask<<mUp))^(rvr<<mUp)));
            _setFlags(&pCri->Flags, DOWN, (BOOL) ((diData & (diMask<<mDown))^(rvr<<mDown)));
            _setFlags(&pCri->Flags, LEFT, (BOOL) ((diData & (diMask<<mLeft))^(rvr<<mLeft)));
            _setFlags(&pCri->Flags, RIGHT, (BOOL) ((diData & (diMask<<mRight))^(rvr<<mRight)));
            lostconn=0;
        }
        _setFlags(&pCri->Flags, LOSTCONNECT, lostconn>3);
        if(debug && ((time(NULL)-isNow) >= 5))
        {
            if(cc<0) cc = (int)timeout;
            printf("%lu|����:%02u|�������:%04lu|WE:%s|Fl:0x%lX|Di:%02X|Station:%lu|Answ:%s\n",time(NULL),num,cc,pCri->WE,(DWORD)pCri->Flags,rvr>0 ? diData^0xFF : diData,(DWORD)pCri->ID_Station,Data);
            isNow=time(NULL);
        }
        Sleep(250UL);
    }
    if(!debug)
        _closeConsole();
    _setFlags(&pCri->Flags, LOSTCONNECT, 1);
    /*	InterlockedExchange8((PBYTE) &pWi->lnkLogik,0);
    	InterlockedExchange8((PBYTE) &pWi->Bell,0);
    	InterlockedExchange8((PBYTE) &pWi->Light,0);
    	InterlockedExchange8((PBYTE) &pWi->Semaphore,0);*/
    CloseHandle(isRun);
    cc=UDP_Close();
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
