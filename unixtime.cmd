call:2UTC "%date% %time%
@set timestamp=%utc%
@echo %timestamp% 
@goto end

:2UTC    - ��������� unixtime. ������� ���������="dd.mm.yyyy HH:MM:SS.......
@REM - �������� ���������� = %utc%. ������� ����� �� �����������. ��������:   "dd.mm.yyyy HH:MM

@set ts=%1
@set ts=%ts:~1%
@set /a "yy=10000%ts:~6,4% %% 10000, mm=100%ts:~3,2% %% 100, dd=100%ts:~0,2% %% 100, H=%ts:~11,2%"
@set /a "M=100%ts:~14,2% %%100, S=100%ts:~17,2% %%100"
@set /a z=(14-mm)/12, y=yy-z, utc=y*365+y/4-y/100+y/400+(153*(mm+12*z-3)+2)/5+dd-719469, utc=utc*86400+H*3600+M*60+S-25200
@goto end

:end