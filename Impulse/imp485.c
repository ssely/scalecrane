///
/// <meta lang="ru" charset="windows-1251" emacsmode='-*- markdown -*-'>
///

#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef IMPULSE485_C
#define IMPULSE485_C

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\scale.h"
#include "..\inc\udp2mat.h"
#include "..\inc\imp485.h"


#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

//#define VT_WEIGHT 1
//#define VT_ZERO 2
//#define LOSTWE 6

/* =============================================================================
 *
 *                                  ������
 *
 * =============================================================================*/
/// # ������
///
/// ������ ������ ������������ ��� ������ � ����� "�������" �� 485 ���������� ���������
/// ������������� �������� �� �������������.
/// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// ����������� � �� �������������� �� �� �����
///
/// ************************************************************************
/// *                            .-.
/// *                         .-+   |
/// *                     .--+       '---.                    .-------.
/// *                    | ���� ��������� |                  |  �����  |
/// *                     '--------------'                    '-------'
/// *                          ^   ^                              ^
/// *                          |   |                              |
/// *  .------.   Ethernet     |   | Ethernet    .------.  RS-485 |
/// *  |  ��  |<--------------'	    '---------->|  MOXA  |<------'
/// *  '------'                                  '------'
/// *
/// *
/// ************************************************************************
///
/// � �������� ������ ������ ���������� ��������� ���������:
///
/// ���� ��������� 2-�� ������ | �����������&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       |��������
/// ---------------------------|:-----------------:|:----------------------------------------------------
/// __SrcAddr__                | _WORD_            | ����� �����, ������ 0
/// __DstAddr__                | _WORD_            | ����� ����� "�������" -- ����������������� `0xFFFF`
/// __Pid__                    | _BYTE_            | ����� ������ ������������� �� ����� "�������"
/// __Cmd__                    | _BYTE_            | ������� ��� ����� (��. ������������)
/// __Flags__                  | _BYTE_            | ����� ���������� ����� (��. ������������)
/// __Status__                 | _BYTE_            | ��� �������� � ����� "�������" ������ `0x80`
/// __DataLen__                | _WORD_            | ����� ������ ��� ����������� �� �����
/// __Data[DataLen]__          | _BYTE x DataLen_  | ������ ���������� ������������ �� �����
///
///
/// &nbsp;
///
/// ���� ��������� 1-�� ������ | ����������� |��������
/// ---------------------------|:-----------:|:-----------------------------------------------
/// __bSTX__                   | _BYTE_      | ������ ������� ������ �������������� ��������� `0x02`
/// __LenLo__                  | _BYTE_      | ������� ���� ����� ������� ������
/// __LenHi__                  | _BYTE_      | ������� ���� ����� ������� ������
/// __Data__                   | _BYTE_      | ������ ���� ������ 2-�� ������
///
/// ## �������������
///
/// ������ ������ ������������ �� ��������� ������
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
///  imp485.exe -conf=���_ini_�����
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// � __ini__ ����� ������ ������������ ������ __[imp485]__.
/// ������ ������ ������� ����:
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// [imp485]
/// ip="10.9.0.56"
/// cport=4001
/// haddr=0
/// taddr=65535
/// align=1
/// flags=3
/// bright=5
/// delay=1000
/// debug=1
/// exit=0
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
///
/// �������� � ������ | ��������
/// ------------------|--------------------------------------------------------------
/// __ip__            | ����� ���������� ���������� MOXA
/// __cport__         | UDP ���� ����� ������� ���������� ������� � ����� "�������"
/// __haddr__         | ����� ����� ������� ��������� �����
/// __taddr__         | ����� ����� "�������"
/// __align__         | ������������ ������� �� ����� (0/1/2 �����/������/ �� ������)
/// __flags__         | bit^0=1 - ����� �� ���������� �����
///                   | bit^1=1 - ����� �� ��������� ���� Pid
///                   | bit^2=1 - ����� ���������� ����� ������ ���� dst �� ����� ������ �����
/// __bright__        | ������� ������� �� 1 �� 10
/// __delay__         | �������� ����� ��������� ������ �� ����� � �������������
/// __debug__         | ������/������� (1/0) ����������� ����
/// __exit__          | 1 - ���������� ������ ������
///
CHAR ReqString[0x100];
///
/// ## __�������__
///
WORD CountCRC(PBYTE Data, PBYTE Table,int Count, PWORD crc)
{
///
/// ### _������� �������� ����������� ����� ������ 2-�� ������_
///
/// !!! Tip
///     __����������� ����� �������������� �� ��������� ������ 2-�� ������__
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// WORD CountCRC(PBYTE Data, PBYTE Table,int Count, PWORD crc);
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������        | ��������
/// ----------------|-----------------------------------------------------
/// __Data__        | ��������� �� �� �������������� ����� ������� ������
/// __Table__       | ��������� �� ������� CRC
/// __Count__       | ������ ����������������� ������ ������� ������
/// __crc__         | ��������� �� ����������� ����� ������ ������� ������
///
/// __����������__ ����������� ����� ������ ������� ������.
///
/// ������ ���������� � ������ ������� _��������� �� ������� CRC_, ����
/// ������� ���������� ��������� �������:
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
/// BYTE CrcTable[0x100]={
///     0,  94, 188, 226,  97,  63, 221, 131, 194, 156, 126,  32, 163, 253,  31,  65,
///      157, 195,  33, 127, 252, 162,  64,  30,  95,   1, 227, 189,  62,  96, 130, 220,
///       35, 125, 159, 193,  66,  28, 254, 160, 225, 191,  93,   3, 128, 222,  60,  98,
///      190, 224,   2,  92, 223, 129,  99,  61, 124,  34, 192, 158,  29,  67, 161, 255,
///       70,  24, 250, 164,  39, 121, 155, 197, 132, 218,  56, 102, 229, 187,  89,   7,
///      219, 133, 103,  57, 186, 228,   6,  88,  25,  71, 165, 251, 120,  38, 196, 154,
///      101,  59, 217, 135,   4,  90, 184, 230, 167, 249,  27,  69, 198, 152, 122,  36,
///      248, 166,  68,  26, 153, 199,  37, 123,  58, 100, 134, 216,  91,   5, 231, 185,
///      140, 210,  48, 110, 237, 179,  81,  15,  78,  16, 242, 172,  47, 113, 147, 205,
///       17,  79, 173, 243, 112,  46, 204, 146, 211, 141, 111,  49, 178, 236,  14,  80,
///      175, 241,  19,  77, 206, 144, 114,  44, 109,  51, 209, 143,  12,  82, 176, 238,
///       50, 108, 142, 208,  83,  13, 239, 177, 240, 174,  76,  18, 145, 207,  45, 115,
///      202, 148, 118,  40, 171, 245,  23,  73,   8,  86, 180, 234, 105,  55, 213, 139,
///       87,   9, 235, 181,  54, 104, 138, 212, 149, 203,  41, 119, 244, 170,  72,  22,
///      233, 183,  85,  11, 136, 214,  52, 106,  43, 117, 151, 201,  74,  20, 246, 168,
///      116,  42, 200, 150,  21,  75, 169, 247, 182, 232,  10,  84, 215, 137, 107,  53};
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


PBYTE cs;
cs = (PBYTE) crc;
int i;
    for(i=0;i<Count;i++)
    {
    cs[0] = Table[cs[0] ^ Data[i]];
    cs[1] = (cs[1] + (~Data[i])) & 0xFF;
    }
return (*crc);
}

/// ### _������� ��������� ����������� �����_
///
///
/// ������������ ��� ��������� ����� � ����������� ����� ��������� ������
///
/// ������ ��������� ������ ������� ����:
///
/// ****************************************************************************************************************
/// *
/// *.------.-----------------.-----------------.----------------.-----------------.-----------------------.------.
/// *|������|  ����� ������   |   ����� ������  | �������������� | �����������     | �����������           |������|
/// *|������|  (������� 7 ���)|  (������� 7 ���)| ����� 2-��     | �����           | �����                 | �����|
/// *|������|                 |                 | ������         | (������� 7 ���) | (������� 7 ���)       |������|
/// *+------+-----------------+-----------------+----------------+-----------------+-----------------------+------+
/// *| 0x02 |(Len & 0x7F)|0x80|(Len & 0x7F)|0x80|byte Data2[xLen]|(CRC & 0x7F)|0x80|((CRC >>7) & 0x7F)|0x80| 0x03 |
/// *|      |                 |                 |                |                 |                       |      |
/// *+------'-----------------'-----------------'----------------'-----------------'-----------------------'------'
/// *
/// ****************************************************************************************************************
///
/// ������ ������ ������|����� ������ (������� 7 ���)|����� ������ (������� 7 ���)|�������������� ����� 2-�� ������|����������� ����� (������� 7 ���)|����������� ����� (������� 7 ���)|������ ����� ������
/// :------|:------------------|:------------------|:--------------|:------------------|:------------------------|:-------
///  `0x2` |`(Len & 0x7F)|0x80`|`(Len & 0x7F)|0x80`|byte Data2[Len]|`(CRC & 0x7F)|0x80`|`((CRC >>7) & 0x7F)|0x80`| `0x03`
///
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// WORD EncodeWord(WORD val)
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������        | ��������
/// ----------------|-----------------------------------------------------
/// __val __        | ���������� ��� ���������
///
/// __����������__ �������������� �����
///

WORD EncodeWord(WORD val)
{
return ((val & 0x3F80)<<1) | (val & 0x7F) | 0x8080;
}

WORD DecodeWord(WORD val)
{
return((val & 0x7F00)>>1) | (val & 0x7F);
}

int EncodeDataForComm(PBYTE Src, PBYTE Dst, int SrcSize)
{
/// ### _������� ��������� ������ ������� ������_
///
///
/// ��������� ������ ������� ������ �������������� �� ��������� ��������:
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~none
/// 1. ��������� ����������� ����� ������.
/// 2. � ������� ����� b ������������� ������� ��� (�������� b = b ^ 0x80).
/// 3. ���� ������������ ���� b ������ ��� ����� 0x20 � �� ����� 0x7F, �� ��
///    �������� ��� ��������� (b).
/// 4. ���� ������������ ���� ������, ��� 0x20 ��� ����� 0x7F, �� ��
///    ���������� �� 2 �����: 0x7F � (b | 0x80).
///
///        b = Src[i] ^ 0x80;
///        if((b < 0x20) || (b==0x7F))
///            {
///            Dst[j++]=0x7F;
///            Dst[j]=b | 0x80;
///            rc++;
///            }
///        else
///            Dst[j]=b;
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// &nbsp;
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// int EncodeDataForComm(PBYTE Src, PBYTE Dst, int SrcSize);
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������        | ��������
/// ----------------|----------------------------------------------------
/// __Src__         | ��������� �� �� �������������� ����� ������� ������
/// __Dst__         | ��������� �� �������������� ����� ������� ������
/// __SrcSize__     | ������ ����������������� ������ ������� ������
///
/// __����������__ ������ ��������������� ������ ������� ������
///

int rc=0,i,j=0;
BYTE b;
    for(i=0;i<SrcSize;i++)
        {
        b = Src[i] ^ 0x80;
        if((b < 0x20) || (b==0x7F))
            {
            Dst[j++]=0x7F;
            Dst[j]=b | 0x80;
            rc++;
            }
        else
            Dst[j]=b;
        j++;
        rc++;
        }
return rc;
}

///
/// ### _������� ������ ������� ������ ��� ����� �������_
///
///
///
/// ![������� ������ ������ ��� ���������� �����](http://g.gravizo.com/svg?
///  digraph G {
///    size ="8,8";
///    MakeFullPacket [shape=box];
///    MakeFullPacket -> CountCRC;
///    CountCRC ->AddSTX [width=1];
///    AddSTX -> EncodeWordP [width=1];
///    EncodeWordP -> AddEwP -> EncodeDataForComm -> EncodeDataForCommA -> ACountCRC -> AddETX;
///    AddSTX [label="Set first byte 0x02"];
///    AddETX [label="Set last byte 0x03"];
///    EncodeDataForCommA [label="Add EncodeDataForComm"];
///    EncodeWordP [label="EncodeWord PackLen"];
///    AddEwP [label="Add EncodeWord PackLen"];
///    ACountCRC [label="Add CountCRC"];
///  })
///
/// ����� ������� ���������� ������ ��� ����� �������
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// int MakeFullPacket(PLevel2Pack Src, PBYTE Dst);
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������        | ��������
/// ----------------|----------------------------------------------------
/// __Src__         | ��������� �� �� �������������� ����� ������� ������
/// __Dst__         | ��������� �� �������������� ����� ������� ������
///
/// __����������__ ������ ��������������� ������ ������� ������
///

int MakeFullPacket(PLevel2Pack Src, PBYTE Dst)
{
PFullPacket FPack;
PWORD crc;
int i=3,PackSize;
WORD cs=0,PackLen,pl;

FPack = (PFullPacket) Dst;
PackLen=(Src->DataLen+sizeof(Level2Pack)-1);
memset((PVOID) Dst,0,(size_t)PackLen+sizeof(Level2Pack));

cs = 0;
CountCRC((PBYTE)Src,(PBYTE)CrcTable,PackLen,&cs);
FPack->bSTX=0x2;
crc = (PWORD) &FPack->LenLo;
*crc=EncodeWord(PackLen);
PackSize=EncodeDataForComm((PBYTE)Src,(PBYTE)(Dst+i),PackLen);
i+=PackSize;
PackSize+=6;
crc = (PWORD)((PBYTE)Dst+i);
*crc=EncodeWord(cs);
i+=2;
Dst[i]=0x3;
return PackSize;
}

INT16 _impulsestring(PLevel2Pack pLp,UINT16 src,UINT16 dst,BYTE flags,BYTE align,PBYTE data)
{
/// ### _������� ������ ������ �� ����� Impulse 485_
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// INT16 _impulsestring(PLevel2Pack pLp,UINT16 src,UINT16 dst,BYTE flags,BYTE align,PBYTE data);
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������    | ��������
/// ------------|--------------------------------------------------------------
/// __pLp__     | ��������� �� ��������� ������ 2-�� ������
/// __src__     | ����� ��������� -- `0x000` �� ���������
/// __dst__     | ����� ����� -- `0xFFFF` ����������������� �����
/// __flags__   | bit^0=1 -- ����� �� ���������� �����
///             | bit^1=1 -- ����� �� ��������� ���� Pid
///             | bit^2=1 -- ����� ���������� ����� ������ ���� dst �� ����� ������ �����
/// __align__   | ������������ ������� (0/1/2 �����/������/ �� ������)
/// __data__    | ��������� �� ������ ��� ������ �� ����� "�������"
///
/// __����������__ ����� ������ ��� ������ ������

INT16 i,len;
PBYTE pack;
len = lstrlen((LPCSTR)data);
    pLp->SrcAddr=src;
    pLp->DstAddr=dst;
    pLp->PId=0x0;
    pLp->Cmd=0x5;
    pLp->Flags=flags;
    pLp->Status=0x80;
    pLp->DataLen=len+9;
    pack = &pLp->Data;
    pack[0]=0x0;
    pack[1]=0x5;
    pack[2]=align;
    pack[3]=0x0;
    pack[4]=0x0;
    pack[5]=0x1;
    pack[6]=0x0;
    pack[7]=0x0;
    pack[8]=0x0;
    lstrcpy((LPSTR)(pack+9),(LPCSTR)data);
return (INT16) pLp->DataLen;
}

INT16 _setbright(PLevel2Pack pLp,UINT16 src,UINT16 dst,BYTE flags,BYTE val)
{
/// ### _������� ��������� ������� ����� Impulse 485_
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// INT16 _setbright(PLevel2Pack pLp,UINT16 src,UINT16 dst,BYTE flags,BYTE val);
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������    | ��������
/// ------------|--------------------------------------------------------------
/// __pLp__     | ��������� �� ��������� ������ 2-�� ������
/// __src__     | ����� ��������� -- `0x000` �� ���������
/// __dst__     | ����� ����� -- `0xFFFF` ����������������� �����
/// __flags__   | bit^0=1 -- ����� �� ���������� �����
///             | bit^1=1 -- ����� �� ��������� ���� Pid
///             | bit^2=1 -- ����� ���������� ����� ������ ���� dst �� ����� ������ �����
/// __val__     | ������� ������� �� 1 �� 10
///
/// __����������__ �������� ������ �������

static BYTE Id=0x0;
    pLp->SrcAddr=src;
    pLp->DstAddr=dst;
    pLp->PId = (Id==0) ? 83:Id++;
    pLp->Cmd=0x2;
    pLp->Flags=flags;
    pLp->Status=0x80;
    pLp->DataLen=1;
    pLp->Data = (val>0) ? val:1;

return val;
}

/// ### _������� ���������� ���������� ��������� ����� �� ini �����_
///
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~c
/// int _get_param(PCHAR ipaddr,PWORD cmdport,PWORD ha,PWORD ta,PBYTE agn,PBYTE flg,PBYTE brg,PDWORD timeout,PBYTE num, BOOL* debug, BOOL* quit,PCHAR IniFile)
/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///
/// ��������    | ��������
/// ------------|-------------------------------------------------------------------------------------
/// __ipaddr__  | ��������� �� ip ����� ���������� ����������� ����
/// __cmdport__ | ��������� �� UDP ���� ��� ������ ������ �� ����� �������
/// __ha__      | ��������� �� ����� ��������� -- `0x000` �� ���������
/// __ta__      | ��������� �� ����� ����� -- `0xFFFF` ����������������� �����
/// __agn__     | ��������� �� ������������� ���� ������������ -- `0/1/2 �����/������/�� ������`
/// __flags__   | ��������� �� ���� ������
///             | bit^0=1 -- ����� �� ���������� �����
///             | bit^1=1 -- ����� �� ��������� ���� Pid
///             | bit^2=1 -- ����� ���������� ����� ������ ���� dst �� ����� ������ �����
/// __brg__     | ��������� �� ������� ������� �� 1 �� 10
/// __timeout__ | ��������� �������� ����� ��������� ���������� �� ����� �������
/// __num__     | ��������� ����� �����
/// __debug__   | ��������� �� ���������� ��� ������ ���� � ���������� ����������� -- 1 ������� ����
/// __quit__    | ��������� �� ���������� ��� ��������� ������ ������ -- 1 ��������� ������ ������
/// __IniFile__ | ��������� ��� ����� ������������
///
/// __���������__ ������������� ���������� ���������� �� _ini_ �����, _��. &sect;1.1_

int _get_param(PCHAR ipaddr,PWORD cmdport,PWORD ha,PWORD ta,PBYTE agn,PBYTE flg,PBYTE brg,PDWORD timeout,PBYTE num, BOOL* debug, BOOL* quit,PCHAR IniFile)
{
CHAR ip[0x10]={0};  // ip ����� ����
CHAR CurDir[0x100];

static BOOL isFirst=FALSE;
int rc=0,cc;

GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);
GetPrivateProfileString("imp485","ip","127.0.0.1",ip,15,(LPCSTR)CurDir);

    if(lstrcmp((LPCSTR)ip,(LPCSTR)ipaddr))
    {
        lstrcpy((LPSTR)ipaddr,(LPCSTR)ip);
        rc|=1;
    }

*cmdport=(WORD)GetPrivateProfileInt("imp485","cport",4001,(LPCSTR)CurDir);
*ha=(WORD)GetPrivateProfileInt("imp485","haddr",0,(LPCSTR)CurDir);
*ta=(WORD)GetPrivateProfileInt("imp485","taddr",0xFFFF,(LPCSTR)CurDir);
*agn=(BYTE)GetPrivateProfileInt("imp485","align",1,(LPCSTR)CurDir);
*flg=(BYTE)GetPrivateProfileInt("imp485","flags",3,(LPCSTR)CurDir);
*brg=(BYTE)GetPrivateProfileInt("imp485","bright",5,(LPCSTR)CurDir);
*timeout=(DWORD)GetPrivateProfileInt("imp485","delay",1000,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("scale","num",1,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("imp485","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
if(!isFirst)
{
	wsprintf(ReqString,"Debug Impulse 485 Request Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) ReqString );
	printf("������������ ����� �������_485 �� �����: %s\n",(PCHAR)CurDir);
	printf("IP �����:\t\t\t%s\n",(PCHAR)ip);
	printf("���� ������:\t\t\t%u\n",(WORD) *cmdport);
	printf("����� �����:\t\t\t%u\n",(WORD) *ha);
	printf("����� �����:\t\t\t%u\n",(WORD) *ta);
	printf("������������:\t\t\t%s\n", (*agn) == 0 ? "�����" : (*agn) == 1 ? "������" : "�� ������");
	printf("�����:\t\t\t\t%u\n",(UINT) *flg);
	printf("������� (0-10):\t\t\t%u\n",(UINT) *brg);
	printf("������� �������� ������:\t%lu ms\n", *timeout);
    printf("����� �����:\t\t\t%03d\n",(INT) *num);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(3000UL);
}

if(!(isFirst=*debug))
		_closeConsole();

*quit=(BOOL)GetPrivateProfileInt("imp485","exit",0,(LPCSTR)CurDir);
return rc;
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    DWORD isNow;
    CHAR ipaddr[0x10]; // ����� ����
    WORD cport; // ���� ��� ������ ������ �� ����� �������
	WORD haddr; // ����� ����� ������� ��������� ����� �������
	WORD taddr; // ����� ����� �������
	BYTE align; // ������������ ������ �� �����
	BYTE flags; // ����� ���������� �����
	BYTE bright; // ������� �����
    DWORD timeout; //������� ������ �� �����
    BYTE num;   // ����� ������������ �����
    BOOL debug=FALSE; // ������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc,rc,ErrorLink=0;
    FileMap fm;
	pCRANEINFO cri;
    PBYTE dataExchange;
    CHAR cfg[0x40];
    BYTE Buffer[0x40];
	HANDLE isRun;
    PLevel2Pack pl2p;
    FullPacket fp;
    PBYTE sendData,pk,sData;


if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
 		return FALSE;


cc=_get_param((PCHAR) ipaddr,&cport,&haddr,&taddr,&align,&flags,&bright,&timeout,&num,&debug, &quit,cfg);


wsprintf((LPSTR)ReqString,"Global\\Impulse%03d",num);
if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
		wsprintf(ReqString,"Debug Impulse 485 Request Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) ReqString );
        printf("��������� Tablo -conf=%s  ��� ��������. �������...",cfg);
    Sleep(3000UL);
    _closeConsole();
    return FALSE;
    }

if(debug)
	printf("Mutex %s is open\n",ReqString);

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
cri=(pCRANEINFO)fm.dataPtr;

if(debug)
	printf("Exchange Object is open:%p\n",dataExchange);

if(cri->ID!=num)
	InterlockedExchange16((volatile SHORT *) &cri->ID,(WORD)num);

	cc=UDP_Init();
if(debug)
	printf("Udp Init:%d\n",cc);

    cc=UDP_OpenSend();
if(debug)
	printf("Udp Send Init:%d\n",cc);

  pl2p = (PLevel2Pack) malloc_w(sizeof(Level2Pack)+0x400);
  sendData = (PBYTE) malloc_w(0x400);

isNow=time(NULL);
InterlockedExchange((volatile LONG *) &cri->Flags,0UL);
_chkConfig((LPSTR) cfg);

rc=_setbright(pl2p,haddr,taddr,flags,bright);
rc = MakeFullPacket((PLevel2Pack) pl2p, sendData);
rc=UDP_Send((PCHAR)ipaddr,(int) cport,sendData,rc);

while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
			cc=_get_param((PCHAR) ipaddr,&cport,&haddr,&taddr,&align,&flags,&bright,&timeout,&num,&debug, &quit,cfg);
			rc=_setbright(pl2p,haddr,taddr,flags,bright);
			rc = MakeFullPacket((PLevel2Pack) pl2p, sendData);
			rc=UDP_Send((PCHAR)ipaddr,(int) cport,sendData,rc);
            if(cc & 0x2){
                if(debug){
                    sprintf(ReqString,"Debug Impulse 485 Request Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) ReqString );
                    printf("Impulse 485 �����:%s Impulse ��������� ����:%u\n",ipaddr,cport);
                }
                else
					_closeConsole();
            }
        }


    if((_getFlags(&cri->Flags, LOSTWE)) || (!cri->WE[0]))
        lstrcpy((LPSTR)Buffer,"------");
    else
        lstrcpy((LPSTR)Buffer,(LPCSTR) cri->WE);

	rc = _impulsestring(pl2p,haddr,taddr,flags,align,(PBYTE) Buffer);
	rc = MakeFullPacket((PLevel2Pack) pl2p, sendData);
	rc=UDP_Send((PCHAR)ipaddr,(int) cport,sendData,rc);

	if(debug && ((time(NULL)-isNow)>=2))
		{
		isNow=time(NULL);
		printf("%lu|����:%02u|Fl:0x%lX|%s|\n",isNow,num,cri->Flags,Buffer);
    	}
	Sleep(timeout);
	}

if(!debug)
		_closeConsole();

    free_w(sendData);
    free_w(pl2p);
	cc=UDP_Close();
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // IMPULSE485_C
